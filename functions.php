<?php

require_once 'inc/assets.php';
require_once 'inc/breadcrumbs.php';
require_once 'inc/helpers.php';
require_once 'inc/custom.php';
require_once 'inc/field-options.php';
require_once 'inc/inventory.php';
require_once 'inc/stm-child-ajax.php';
require_once 'inc/customizer.php';
require_once 'inc/custom-url.php';
require_once 'inc/subscriptions.php';
require_once 'inc/vc-elements.php';
require_once 'inc/woocomerce.php';
require_once 'inc/algolia.php';
require_once 'inc/custom_parent-tax.php';
require_once 'inc/is_login.php';
require_once 'inc/recaptcha.php';
require_once 'inc/app-ajax.php';
require_once 'inc/cron.php';
