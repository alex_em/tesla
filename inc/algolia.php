<?php
function algolia_post_to_record(WP_Post $post) {
	$tags = array_map(function (WP_Term $term) {
		return $term->name;
	}, wp_get_post_terms( $post->ID, 'post_tag' ));
	$img_id = get_post_meta($post->ID, '_thumbnail_id', true);
	$gallery = get_post_meta($post->ID, 'gallery',true);
	$size = 'stm-img-796-466';
	$thumbnail[] = wp_get_attachment_image_src($img_id, $size)[0];
	$gallery_photos = getPostGalleryUrls($gallery, $size);
	$car_photos = array_merge($thumbnail,$gallery_photos);
	/** @var WP_Term $ext_color */
	$ext_color = wp_get_post_terms($post->ID, 'exterior-color');
	$ext_color = reset( $ext_color );
	$ext_colors = [ $ext_color->name ];
	if ( $ext_color->parent ) {
		array_unshift( $ext_colors, get_term( $ext_color->parent )->name );
	}

	return [
		'objectID' => implode('#', [$post->post_type, $post->ID]),
		'title' => $post->post_title,
		'author' => [
			'id' => $post->post_author,
			'name' => get_user_by( 'ID', $post->post_author )->display_name,
		],
		'excerpt' => $post->post_excerpt,
		'content' => strip_tags($post->post_content),
		'tags' => $tags,
		'url' => get_post_permalink($post->ID),
		'post_id' => $post->ID,
		'for_faceting' => [
			'year' =>  get_post_meta($post->ID, 'model-year', true),
			'model' => wp_get_post_terms($post->ID, 'model', array('fields' => 'names'))[0],
			'modelTrim' => wp_get_post_terms($post->ID, 'model-specific-trim', array('fields' => 'names'))[0],
			'extColor' => $ext_colors,
			'intColor' => wp_get_post_terms($post->ID, 'interior-color', array('fields' => 'names'))[0],
			'autopilotSoftware' => wp_get_post_terms($post->ID, 'autopilot-software', array('fields' => 'names'))[0],
			'autopilotHardware' => wp_get_post_terms($post->ID, 'autopilot-hardware', array('fields' => 'names'))[0],
			'battery' => wp_get_post_terms($post->ID, 'battery', array('fields' => 'names'))[0],
			'drive' => wp_get_post_terms($post->ID, 'drive', array('fields' => 'names'))[0],
			'performance' => wp_get_post_terms($post->ID, 'performance', array('fields' => 'names'))[0],
			'seating' => wp_get_post_terms($post->ID, 'seating', array('fields' => 'names'))[0],
			'dash' => wp_get_post_terms($post->ID, 'dash', array('fields' => 'names'))[0],
			'wheels' => wp_get_post_terms($post->ID, 'wheels', array('fields' => 'names'))[0],
			'sellerType' => wp_get_post_terms($post->ID, 'seller-type', array('fields' => 'names'))[0],
		],
		'extColor' => $ext_color->name,
		'thumbnail' => wp_get_attachment_image_src($img_id, 'stm-img-796-466')[0],
		'price' => (float)get_post_meta($post->ID, 'price', true),
		'mileage' => (float)str_replace(',', '', get_post_meta($post->ID, 'mileage', true)),
		'date' => (float)get_the_date('U', $post->ID),
		'_geoloc' => [
			'lat' => (float)get_post_meta($post->ID, 'stm_lat_car_admin', true),
			'lng' => (float)get_post_meta($post->ID, 'stm_lng_car_admin', true),
		],
		'state' => get_post_meta($post->ID, 'stm_state_car_admin', true),
		'galleryUrls' => $car_photos,
		'model' => wp_get_post_terms($post->ID, 'model', array('fields' => 'names'))[0],
	];
}
add_filter('post_to_record', 'algolia_post_to_record');


//add_action( 'save_post_listings', 'my_activation', 10, 3 );
function my_activation($id, WP_Post $post, $update) {
		wp_schedule_single_event( time() + 5, 'my_new_event', array($id, $post, $update) );
}

add_action( 'my_new_event','algolia_update_post', 10, 3 );
function algolia_update_post($id, WP_Post $post, $update) {

	if (wp_is_post_revision( $id) || wp_is_post_autosave( $id )) {
		return $post;
	}

	global $algolia;

	$record = (array) apply_filters('post_to_record', $post);

	if (! isset($record['objectID'])) {
		$record['objectID'] = implode('#', [$post->post_type, $post->ID]);
	}

	$index = $algolia->initIndex(
		apply_filters('algolia_index_name', 'listings')
	);

	if ('trash' == $post->post_status || 'draft' == $post->post_status || 'pending' == $post->post_status) {
		$index->deleteObject($record['objectID']);
	} else {
		$index->saveObject($record);
	}
	return $post;
}



function algolia_update_post_meta($meta_id, $object_id, $meta_key, $_meta_value) {
	global $algolia;
	$indexedMetaKeys = ['seo_description', 'seo_title', 'price', 'mileage', 'title' ];

	if (in_array($meta_key, $indexedMetaKeys)) {
		$index = $algolia->initIndex(
			apply_filters('algolia_index_name', 'listings')
		);

		$index->partialUpdateObject([
			'objectID' => 'listings#'.$object_id,
			$meta_key => $_meta_value,
		]);
	}
}

add_action('update_post_meta', 'algolia_update_post_meta', 10, 4);


function algolia_terms_hierarchy() {
	$taxonomies = get_object_taxonomies( 'listings', '' );
	$taxonomies = wp_filter_object_list( $taxonomies, [ 'hierarchical' => true ] );

	$terms = get_terms( [ 'taxonomy' => array_keys( $taxonomies ), 'hide_empty' => false ] );
	$terms = wp_list_pluck( $terms, 'name', 'term_id' );

	$result = [];
	foreach ( array_keys( $taxonomies ) as $taxonomy ) {
		$result[ $taxonomy ] = [];
		foreach ( _get_term_hierarchy( $taxonomy ) as $parent_id => $children_ids ) {
			$parent = $terms[ $parent_id ];
			foreach ( $children_ids as $term_id ) {
				$result[ $taxonomy ][ $terms[ $term_id ] ] = $parent;
			}
		}
	}

	return $result;
}
