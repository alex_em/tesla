<?php

remove_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');
remove_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_ajax_get_seller_phone');
add_action('wp_ajax_stm_ajax_get_seller_phone', 'stm_child_ajax_get_seller_phone');
add_action('wp_ajax_nopriv_stm_ajax_get_seller_phone', 'stm_child_ajax_get_seller_phone');

function stm_child_ajax_get_seller_phone()
{
	check_ajax_referer('stm_ajax_get_seller_phone', 'security');
	$recaptchaValidation = recaptchaValidation($_GET['recaptcha-token']);

	if ( $recaptchaValidation['success'] ) {
		$response['phone'] = get_user_meta($_GET["phone_owner_id"], 'stm_phone', true);
        $response['recaptcha'] = $recaptchaValidation;
	} else {
		$response['error'] = __('Recaptcha validation failed!', 'motors-child');
	}

	wp_send_json($response);
	exit;
}

add_action( 'wp_ajax_custom_photo_upload', 'custom_photo_upload' );
add_action( 'wp_ajax_nopriv_custom_photo_upload', 'custom_photo_upload' );

function custom_photo_upload()
{
    $updating = !empty($_POST['stm_edit']) and $_POST['stm_edit'] == 'update';
    $error = false;
	$response = [];

    $files_approved = array();

    if (!empty($_FILES)) {

        $max_file_size = apply_filters('stm_listing_media_upload_size', 1024 * 4000); /*4mb is highest media upload here*/

        foreach ($_FILES['files']['name'] as $f => $name) {
           if ($_FILES['files']['error'][$f] != UPLOAD_ERR_OK) {
                $error = true;
            } else {
                // Check if the file being uploaded is in the allowed file types
                $check_image = @getimagesize($_FILES['files']['tmp_name'][$f]);
                if ($_FILES['files']['size'][$f] > $max_file_size) {
                    $response['message'] = esc_html__('Sorry, image is too large', 'stm_vehicles_listing') . ': ' . $name;
                    $error = true;
                } elseif (empty($check_image)) {
                    $response['message'] = esc_html__('Sorry, image has invalid format', 'stm_vehicles_listing') . ': ' . $name;
                    $error = true;
                } else {
                    $tmp_name = $_FILES['files']['tmp_name'][ $f ];
                    $error = $_FILES['files']['error'][ $f ];
                    $type = $_FILES['files']['type'][ $f ];
                    $files_approved[$f] = compact('name', 'tmp_name', 'type', 'error');
                }
            }
        }

    }

    require_once(ABSPATH . 'wp-admin/includes/image.php');

    foreach ($files_approved as $f => $file) {
        $uploaded = wp_handle_upload($file, array('action' => 'custom_photo_upload'));

        if ($uploaded['error']) {
            $response['errors'][ $file['name'] ] = $uploaded;
            continue;
        }

        $filetype = wp_check_filetype(basename($uploaded['file']), null);

        // Insert attachment to the database
        $attach_id = wp_insert_attachment(array(
            'guid' => $uploaded['url'],
            'post_mime_type' => $filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($uploaded['file'])),
            'post_content' => '',
            'post_status' => 'inherit',
        ), $uploaded['file']);

        add_post_meta($attach_id, 'attachment_to_delete', $attach_id);

        update_post_meta($attach_id, 'blob', $_POST['blob']);

		$response = [
			'src' => wp_get_attachment_image_src($attach_id, 'stm-img-796-466'),
			'thumb' => wp_get_attachment_image_src($attach_id, 'stm-img-255-160'),
			'index' => $_POST['lastUploadedIndex'],
			'id'   => $attach_id
		];
    }

    wp_send_json($response);
    exit;
}

