<?php
$theme_info = wp_get_theme();

add_action('wp_enqueue_scripts', 'stm_enqueue_child_styles');
add_action('wp_enqueue_scripts', 'algolia_load_assets');
add_action('wp_print_scripts', 'deq_der_script', 100);

function stm_enqueue_child_styles()
{
	$recaptcha_site_key = get_theme_mod('recaptcha_public_key');
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', ['stm-theme-style']);
	enqueue_mix_style( 'main.css', 'css/main.css', [ 'stm-theme-style' ] );
	wp_enqueue_style('fancybox-3-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css', ['child-style']);
	wp_enqueue_script('abid-script', get_stylesheet_directory_uri() . '/assets/js/abid-script.js', ['jquery'], '1.0', true);
	wp_enqueue_script('cleave-js', get_stylesheet_directory_uri() . '/assets/js/cleave/cleave.min.js', ['jquery'], '1.0', true);
	wp_register_script('mileage-js', get_stylesheet_directory_uri() . '/assets/js/cleave/mileage-comma.js', ['jquery'], '1.0', true);
	wp_register_script('price-js', get_stylesheet_directory_uri() . '/assets/js/cleave/price-comma.js', ['jquery'], '1.0', true);
	wp_enqueue_script('vin-validator-js', get_stylesheet_directory_uri() . '/assets/js/vin-validator/tesla-vin-validator.js', ['jquery'], '1.0', true);
	wp_enqueue_script('animatescroll', get_stylesheet_directory_uri() . '/assets/js/animatescroll.min.js', ['jquery-ui-slider'], '1.0', false);
    wp_register_script( 'stm_recaptchav3','https://www.google.com/recaptcha/api.js?render='. $recaptcha_site_key, [], false);
    wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/assets/js/custom.js', ['jquery-ui-slider'],  false, false);
    wp_localize_script( 'custom-script', 'recaptcha_site_key', $recaptcha_site_key);
    wp_enqueue_script('child-google-places', get_stylesheet_directory_uri() . '/assets/js/child-google-places.js', 'stm_gmap', STM_THEME_VERSION, true);
    wp_enqueue_script('fancybox-3', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js');
    wp_deregister_script('wc-cart');
	register_mix_script('stm-ajax', 'js/stm-ajax.js', ['stm-theme-scripts-ajax'] );
	wp_localize_script( 'stm-ajax', 'recaptcha_site_key', $recaptcha_site_key);

	/*the only way make redirect after cart empty is overwrite woo js file (wc-cart.js)*/
	wp_register_script('wc-cart', get_stylesheet_directory_uri() . '/assets/js/wc-cart-child.js', ['jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'], WC_VERSION);
	wp_register_script('slide-panel', get_stylesheet_directory_uri() . '/assets/js/slide-in-panel.js', ['jquery'], WC_VERSION);

	if (is_cart()) {
		wp_enqueue_script('wc-cart');
	}

	register_mix_script('add-listing', 'js/add-listing.js', [], true);

	wp_register_script('tinymce-cdn', 'https://cdn.tiny.cloud/1/y9vn2jovf42kw8tuiyphme0bqqz0uk6kl1z7my4iau9xv2fd/tinymce/5/tinymce.min.js');
	register_mix_script('tinymce', 'js/tinymce.js', [], false);

	$login_register_page = (int) get_theme_mod('login_page');
	if (is_singular('listings') || is_page([$login_register_page])) {
		wp_enqueue_script('stm_recaptchav3');
		wp_enqueue_script('stm-ajax');
	}

	wp_enqueue_style('photoswipe-css', 'https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.css', [], STM_THEME_VERSION);
	wp_enqueue_style('photoswipe-default-css', 'https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/default-skin/default-skin.min.css', [], STM_THEME_VERSION);;
	wp_enqueue_script('photoswipe-ui-default-js', get_stylesheet_directory_uri() . '/assets/js/deff.js', ['stm-theme-scripts'], STM_THEME_VERSION);
	wp_enqueue_script('cascading-dropdown',
		get_stylesheet_directory_uri() . '/assets/js/cascading-dropdown.js', ['stm-theme-scripts'], STM_THEME_VERSION, true);
}

function algolia_load_assets() {
	$clientPath = '/assets/js/vendor/algoliasearchLite.min.js';
	$isPath = '/assets/js/vendor/instantsearch.production.min.js';

	// create version number based for the last time the file was modified
	$clientVersion  = date("ymd-Gis", filemtime( get_stylesheet_directory() . $clientPath ));
	$isVersion  = date("ymd-Gis", filemtime( get_stylesheet_directory() . $isPath ));

	wp_enqueue_script( 'algolia-client', get_stylesheet_directory_uri() . $clientPath, array(), $clientVersion, true );
	wp_enqueue_script( 'algolia-instant-search', get_stylesheet_directory_uri() . $isPath, array('algolia-client'), $isVersion, true );

	register_mix_script( 'algolia-search', 'js/src/algolia-search.js', [], true );
	wp_localize_script( 'algolia-search', 'algoliaTermsHierachy', algolia_terms_hierarchy() );
}

function deq_der_script()
{
	wp_dequeue_script('stm-google-places');
	wp_deregister_script('stm-google-places');
}

// Registering assest with Laravel Mix

function register_mix_script( $handle, $path, $deps = [], $in_footer = true ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_register_script( $handle, get_stylesheet_directory_uri() . '/assets' . $path, $deps, $version, $in_footer );
}

function enqueue_mix_script( $handle, $path, $deps = [], $in_footer = true ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_enqueue_script( $handle, get_stylesheet_directory_uri() . '/assets' . $path, $deps, $version, $in_footer );
}

function register_mix_style( $handle, $path, $deps = [], $media = 'all' ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_register_style( $handle, get_stylesheet_directory_uri() . '/assets' . $path, $deps, $version, $media );
}

function enqueue_mix_style( $handle, $path, $deps = [], $media = 'all' ) {
	$path = '/' . ltrim( $path );
	$version = mix_version( $path );

	wp_enqueue_style( $handle, get_stylesheet_directory_uri() . '/assets' . $path, $deps, $version, $media );
}

function mix_version( $path ) {
	$manifest = mix_manifest();
	if ( isset( $manifest[ $path ] ) ) {
		$version = str_replace( $path . '?id=', '', $manifest[ $path ] );
	} else {
		$version = STM_THEME_VERSION;
	}

	return $version;
}

function mix_manifest() {
	static $manifest;

	if ( is_null( $manifest ) ) {
		$manifest = json_decode( file_get_contents( get_stylesheet_directory() . '/assets/mix-manifest.json' ), true );
	}

	return $manifest;
}
