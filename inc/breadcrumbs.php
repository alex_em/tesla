<?php

function custom_breadcrubms()
{

    $all_packages_page_id = (int)get_theme_mod('all_packages');
    $add_a_car_page = (int)get_theme_mod('user_add_car_page');
    $inventory_page = (int)get_theme_mod('listing_archive');

    if (is_singular('listings')) {
        return '<span property="itemListElement" typeof="ListItem">
                      <a property="item" typeof="WebPage" title="Back To Listings" href="' .get_permalink($inventory_page).'" class="home">
                      &lt; <span property="name">Back To Listings</span>
                      </a><meta property="position" content="1">
                  </span>';
    }
    if (is_page($all_packages_page_id)) {
        $add_a_car_page = (int)get_theme_mod('user_add_car_page');
        return '<span class="custom-breadcrumbs" property="itemListElement" typeof="ListItem">
                  <a property="item" typeof="WebPage" title="Go to edit listing" 
                  style="text-decoration: underline !important;"
                  href="' . get_permalink($add_a_car_page) . '" class="home">
                    <span property="name">Add Listing</span>
                  </a>
                <meta property="position" content="1"></span> &gt; 
                <span class="post post-page current-item">Select Package</span>';
    }

    if (is_cart()) {
        return '<span class="custom-breadcrumbs" property="itemListElement" typeof="ListItem">
                  <a property="item" typeof="WebPage" title="Go to edit listing" 
                  style="text-decoration: underline !important;"
                  href="' . get_permalink($add_a_car_page) . '" class="home">
                    <span property="name">Add Listing</span>
                  </a>
                <meta property="position" content="1"></span> &gt; 
                <a property="item" typeof="WebPage" title="Select package"
                  style="text-decoration: underline !important;" 
                  href="' . get_permalink($all_packages_page_id) . '" class="home">
                    <span property="name">Select Package</span>
                  </a>
                <meta property="position" content="2"></span> &gt;
                <span class="post post-page current-item">Cart</span>';
    }

    return;
}
