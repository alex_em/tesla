<?php

//add_action( 'wp', 'delete_unused_listing_images' );
//add_action('delete_unused_listing_images', 'delete_unused_listing_images');
//add_action( 'wp', function () {
//    if( ! wp_next_scheduled( 'delete_unused_listing_images' ) ) {
//        wp_schedule_event( time(), 'hourly', 'delete_unused_listing_images');
//    }
//});

function delete_unused_listing_images() {
    $current_attachments = get_posts(array(
        'fields' => 'ids',
        'post_type' => 'attachment',
        'post_status' => 'inherit',
        'posts_per_page' => -1,
        'meta_key' => 'attachment_to_delete'
    ));

    if (count($current_attachments) <= 0) {
        return;
    }

    foreach ($current_attachments as $delete_attachment) {
        wp_delete_attachment($delete_attachment, true);
    }
}

