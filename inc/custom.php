<?php

//Remove Coupon Field from Checkout Page
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);

add_filter( 'manage_listings_posts_columns', 'set_custom_edit_book_columns', 10, 1);

function set_custom_edit_book_columns($columns) {

    $columns['status'] = __( 'Status', 'motors-child' );

    return $columns;
}

add_action( 'manage_listings_posts_custom_column' , 'custom_book_column', 10, 2 );
function custom_book_column( $column, $post_id ) {
    if($column === 'status'){
        $status = get_post_meta($post_id, 'status', true);
        echo '<strong style="text-transform: capitalize;">'. str_replace('_', ' ', $status) . '</strong>';
    }
}
add_action('template_redirect', function(){
	$add_car_page = (int) get_theme_mod('user_add_car_page');

	if( !is_user_logged_in() && is_page($add_car_page)){
		$redirect_page =  (int) get_theme_mod( 'user_redirect_page', 1718);
		wp_safe_redirect(get_permalink($redirect_page));
		die;
	}
});

add_action('save_post_listings', 'change_url', 10, 3);
function change_url($post_id, $response, $update)
{
	$post = get_post($post_id);

    $post_name = sanitize_title($post->post_name);
    $new_url = $post_name . '-' . $post_id;

    if (!$post->post_name) {
      return;
    }
    if (!(strpos($post->post_name, '-' . $post_id) === false)) {
      return;
    }

    $post_args = [];
    $post_args['ID'] = $post_id;
    $post_args['post_name'] = $new_url;

    wp_update_post($post_args);

}

add_action('wp_footer', function(){?>
    <script>
      var packages_url = '<?php echo get_home_url() . '/' .all_packages_page();?>'
    </script>
<?php });

add_action( 'wp_footer', function () {
    $all_packages_page_id =  (int) get_theme_mod('all_packages');
    $user_add_car_page =  (int) get_theme_mod('user_add_car_page');

    if (is_cart() || is_page([$all_packages_page_id, $user_add_car_page]) || is_checkout())  {
        return;
    }
    ?>
    <script>
      jQuery(function($) {
        $(document).ready(function() {
          $.cookie("listing_id", '', { path: '/'});
        })
      })
    </script>
<?php } );

add_action('wp_footer', function(){
	if(is_singular('listings')) {
		get_template_part('partials/single-car/photoswipe-layout');
	}
	if(is_page((int) get_theme_mod('user_add_car_page'))) {
	  get_template_part('partials/modals/confirmation-delete');
	}
});
