<?php
/*

For this used this script need include
custom.js

*/

function stm_data_binding_custom($allowAll = false)
{
	$attributes = stm_get_car_parent_exist();

	$bind_tax = array();
	$depends = array();
	foreach ($attributes as $attr) {

		$parent = $attr['listing_taxonomy_parent'];
		$slug = $attr['slug'];


		if (!is_array($parent)) {
			$parent = array($parent);
		}

		foreach ($parent as $par) {
			$depends[] = array('parent' => $par, 'dep' => $slug);

			if (!isset($bind_tax[$par])) {
				$bind_tax[$par] = array();
			}
		}


		$bind_tax[$slug] = array(
			'dependency' => $parent,
			'allowAll' => $allowAll,
			'options' => [],
		);

		/** @var WP_Term $term */

		foreach (stm_get_category_by_slug_all($slug) as $term) {
			$deps = array_values(array_filter((array)get_term_meta($term->term_id, 'stm_parent')));

			$bind_tax[$slug]['options'][] = array(
				'value' => $term->slug,
				'label' => $term->name,
				'count' => $term->count,
				'deps' => $deps,
			);
		}
	}

	$sortDeps = array();

	for ($q = 0; $q < count($depends); $q++) {
		if ($q == 0) {
			$sortDeps[] = $depends[$q]['parent'];
			$sortDeps[] = $depends[$q]['dep'];
		} else {
			if (in_array($depends[$q]['dep'], $sortDeps)) {
				array_splice($sortDeps, array_search($depends[$q]['dep'], $sortDeps), 0, $depends[$q]['parent']);
			} elseif (in_array($depends[$q]['parent'], $sortDeps)) {
				array_splice($sortDeps, array_search($depends[$q]['parent'], $sortDeps) + 1, 0, $depends[$q]['dep']);
			} elseif (!in_array($depends[$q]['parent'], $sortDeps)) {
				array_splice($sortDeps, 0, 0, $depends[$q]['parent']);
				array_splice($sortDeps, count($sortDeps), 0, $depends[$q]['dep']);
			}
		}
	}

	$newBindTax = array();

	foreach ($sortDeps as $val) {
		$newBindTax[$val] = $bind_tax[$val];
	}

	return apply_filters('stm_data_binding', $newBindTax);
}


add_action( 'admin_head', function(){
    ?>
    <style type="text/css">
        .wp-admin select[multiple]{
            height: auto !important;
        }
        div.stm_multi_parent_tax{
            display:inline-block;
            width:100%;
        }
        tr.stm_multi_parent_tax{

        }
        div.stm_multi_parent_tax select,
        tr.stm_multi_parent_tax select{
            max-width: 140px;
        }
        div.stm_multi_parent_tax select option,
        tr.stm_multi_parent_tax select option{
            text-overflow: ellipsis;
            overflow: hidden;
        }
    </style>
    <?php
} );
if ( ! is_admin() ) {
    add_action( 'wp_enqueue_scripts', 'stm_load_theme_ss_child' );
}

function stm_load_theme_ss_child() {

    wp_enqueue_script( 'stm-custom-child', get_stylesheet_directory_uri() . '/assets/js/custom.js', array('jquery','stm-theme-scripts'), STM_THEME_VERSION, true );

}



function stm_vehicle_listings_field_multiselect($name, $settings, $values) { ?>
    <div class="stm_form_wrapper stm_form_wrapper_<?php echo $settings['type']; ?>" <?php stm_vehicles_listing_show_dependency($settings); ?>>
        <span><?php echo $settings['label']; ?></span>
        <select multiple name="<?php echo $name; ?>[]">
            <?php foreach($settings['choices'] as $value => $label):
                if(is_array($values[$name])){
                    $selected = (!empty($values[$name]) and in_array($value, $values[$name])) ? 'selected' : '';
                }else{
                    $selected = (!empty($values[$name]) and $values[$name] == $value) ? 'selected' : '';
                }
                ?>
                <option value="<?php echo $value; ?>" <?php echo $selected; ?>><?php echo $label; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
<?php }



/*Ajax saving single option*/
function stm_child_listings_save_single_option_row(){
    $data = array(
        'error' => false,
        'message' => ''
    );

    $options = stm_listings_get_my_options_list();


    /*Check number of setting*/
    if (!isset($_POST['stm_vehicle_listing_row_position'])) {
        $data['error'] = true;
        $data['message'] = esc_html__('Some error occurred', 'stm_vehicles_listing');
    } else {
        $option_key = intval($_POST['stm_vehicle_listing_row_position']);
    }

    /*Check if setting exists*/
    if (empty($options[$option_key])) {
        $data['error'] = true;
        $data['message'] = esc_html__('Some error occurred', 'stm_vehicles_listing');
    } else {
        $current_option = $options[$option_key];

    }

    /*Check POST*/
    if (empty($_POST)) {
        $data['error'] = true;
        $data['message'] = esc_html__('Some error occurred', 'stm_vehicles_listing');
    } else {
        $user_choice = $_POST;
    }

    if (!$data['error']) {

        $settings = stm_listings_page_options();

        foreach ($settings as $setting_name => $setting) {
            if (strpos($setting_name, 'divider') === false) {
                if (!empty($user_choice[$setting_name])) {
                    $current_option[$setting_name] = sanitize_text_field($user_choice[$setting_name]);
                } else {
                    $current_option[$setting_name] = '';
                }
            }
        }

        if (empty($current_option['listing_rows_numbers_enable'])) {
            $current_option['enable_checkbox_button'] = $current_option['listing_rows_numbers'] = '';
        }

        if(!empty($_POST['listing_taxonomy_parent'])){
            $current_option['listing_taxonomy_parent'] = $_POST['listing_taxonomy_parent'];
        }

        $options[$option_key] = $current_option;


        // Check for empty tax value
        foreach ($options[$option_key]['listing_taxonomy_parent'] as $tax_parent) {
            if(empty($tax_parent)) $options[$option_key]['listing_taxonomy_parent'] = '';
        }


        stm_vehicle_listings_save_options($options);

        $data['error'] = false;
        $data['message'] = esc_html__('Settings saved', 'stm_vehicles_listing');
        $data['data'] = $current_option;
    }

    wp_send_json($data);
    exit;
}
remove_action('wp_ajax_stm_listings_save_single_option_row', 'stm_listings_save_single_option_row');
add_action('wp_ajax_stm_listings_save_single_option_row', 'stm_child_listings_save_single_option_row');


// Reinit List of parent with multiple
add_filter('stm_listings_page_options_filter', function($options){
    $taxes = get_taxonomies( array('object_type' => array('listings')), 'objects' );

    $names = array('' => esc_html__('No parent', 'stm_vehicles_listing'));

    if($taxes && count($taxes)){
        foreach ($taxes as $tax) {
            $names[$tax->name] = $tax->label;
        }
    }

    $options['listing_taxonomy_parent'] = array(
        'label' => esc_html__('Set parent taxonomy', 'stm_vehicles_listing'),
        'value' => '',
        'type' => 'multiselect',
        'choices' => $names
    );

    return $options;
});


// Reinit taxonomy parent for multiple
add_action('wp_loaded', function(){
    $stm_get_car_parent_exist = stm_get_car_parent_exist();
    if (!empty($stm_get_car_parent_exist)) {
        foreach ($stm_get_car_parent_exist as $stm_get_car_parent_exist_single) {
            /** Add Custom Field To Form */
            remove_action($stm_get_car_parent_exist_single['slug'] . '_add_form_fields', 'stm_taxonomy_listing_add_field_parent');
            remove_action($stm_get_car_parent_exist_single['slug'] . '_edit_form_fields', 'stm_taxonomy_listing_edit_field_parent');

            add_action($stm_get_car_parent_exist_single['slug'] . '_add_form_fields', 'stm_child_taxonomy_listing_add_field_parent', 110);
            add_action($stm_get_car_parent_exist_single['slug'] . '_edit_form_fields', 'stm_child_taxonomy_listing_edit_field_parent', 110, 2);
        }
    }
});

// On add term with muptiple parent
function stm_child_taxonomy_listing_add_field_parent($taxonomy){
    $taxonomy = stm_get_all_by_slug($taxonomy);
    $taxonomy_parent_slug = $taxonomy['listing_taxonomy_parent'];

    if(is_array($taxonomy_parent_slug)): ?>
        <div class="form-field stm_multi_parent_tax">
            <?php foreach ($taxonomy_parent_slug as $taxonomy_slug):
                $taxonomy_parent = stm_get_category_by_slug_all($taxonomy_slug);

                $c_tax = get_taxonomies( array('name' => $taxonomy_slug), 'objects' ); ?>
                <span style="float:left">
					<label style="display:block"><?php echo $c_tax[$taxonomy_slug]->label ?></label>
					<select multiple name="stm_parent_taxonomy[]" size="10">
						<option value=""><?php esc_html_e('No parent'); ?></option>
                        <?php if (!empty($taxonomy_parent)): ?>
                            <?php foreach ($taxonomy_parent as $term): ?>
                                <?php
                                $parent = get_term_meta( $term->term_id, 'stm_parent', true );
                                $p_name = '';
                                if(!empty($parent)){
                                    $parent_tax = stm_get_all_by_slug($taxonomy_slug);
                                    if($parent_tax['listing_taxonomy_parent'] && is_array($parent_tax['listing_taxonomy_parent'])){
                                        foreach ($parent_tax['listing_taxonomy_parent'] as $dep_tax) {
                                            $p_name = get_term_by( 'slug', $parent, $dep_tax );
                                            $p_name = $p_name->name;
                                            if($p_name && !is_wp_error( $p_name )) break;
                                        }
                                    }
                                }
                                $name = apply_filters('stm_parent_taxonomy_option', $term->name.(!empty($p_name) ? " ($p_name)" : ""), $term, $taxonomy);
                                ?>
                                <option	title="<?php echo $name ?>"	value="<?php echo esc_attr($term->slug) ?>">
									<?php echo $name ?>
								</option>
                            <?php endforeach; ?>
                        <?php endif; ?>
					</select>
				</span>

            <?php endforeach ?>
        </div>
    <?php endif;
}

// On edit term with multiple parent
function stm_child_taxonomy_listing_edit_field_parent($tag, $taxonomy){
    $values = get_term_meta( $tag->term_id, 'stm_parent' );
    $taxonomy = stm_get_all_by_slug($taxonomy);
    if(is_array($taxonomy['listing_taxonomy_parent'])): ?>
        <tr class="form-field stm_multi_parent_tax">
            <th scope="row" valign="top"><label	for="stm_parent_taxonomy"><?php esc_html_e('Parent category'); ?></label></th>
            <td>
                <?php foreach ($taxonomy['listing_taxonomy_parent'] as $taxonomy_slug): ?>
                    <?php $parents = stm_get_category_by_slug_all( $taxonomy_slug ); ?>
                    <?php $c_tax = get_taxonomies( array('name' => $taxonomy_slug), 'objects' ); ?>
                    <span style="float:left">
						<label style="display:block"><?php echo $c_tax[$taxonomy_slug]->label ?></label>
						<select multiple name="stm_parent_taxonomy[]" size="10">
							<option value=""><?php esc_html_e('No parent'); ?></option>
                            <?php if (!empty($parents)): ?>
                                <?php foreach ($parents as $term): ?>
                                    <?php
                                    $parent = get_term_meta( $term->term_id, 'stm_parent', true );
                                    $p_name = '';
                                    if(!empty($parent)){
                                        $parent_tax = stm_get_all_by_slug($taxonomy_slug);
                                        if($parent_tax['listing_taxonomy_parent'] && is_array($parent_tax['listing_taxonomy_parent'])){
                                            foreach ($parent_tax['listing_taxonomy_parent'] as $dep_tax) {
                                                $p_name = get_term_by( 'slug', $parent, $dep_tax );
                                                $p_name = $p_name->name;
                                                if($p_name && !is_wp_error( $p_name )) break;
                                            }
                                        }
                                    }

                                    $name = apply_filters('stm_parent_taxonomy_option', $term->name, $term, $taxonomy);
                                    ?>
                                    <option value="<?php echo esc_attr( $term->slug ) ?>" title="<?php echo $name ?>" <?php selected( in_array( $term->slug, $values ) ); ?>>
										<?php echo $name; ?>
									</option>
                                <?php endforeach; ?>
                            <?php endif; ?>
						</select>
					</span>
                <?php endforeach; ?>

            </td>
        </tr>
    <?php endif;
}

add_filter('stm_data_binding', function($newBindTax){
    foreach ($newBindTax as $key => $value) {
        if($value == null) $newBindTax[$key] = array();
    }

    return $newBindTax;
});

