<?php

function getPostGalleryUrls($gallery, $size)
{
    if (!$gallery) {
        return [];
    }

    $car_photos = [];
    foreach ($gallery as $key => $value) {
        $car_photos[] = wp_get_attachment_image_url($value, $size);
    }
    return $car_photos;
}
 /*
  	src: '',
	preload: false,
	index: this.previews.length + 1,
	progress: 0,
 */
function gallery_images($post_id) {
	if (!$post_id) {
		return [];
	}

	$car_photos = [];
	$gallery = get_post_meta($post_id, 'gallery', true);
	$thumb = get_post_thumbnail_id($post_id) ?? '';

	if ($thumb) {
		array_unshift($gallery, $thumb);
	}

	foreach ($gallery as $k => $photo_id) {
//	    $blob = get_post_meta($photo_id, 'blob', true);
		$car_photos[$k]['id'] = $photo_id;
		$car_photos[$k]['index'] = $k;
		$car_photos[$k]['preload'] = false;
		$car_photos[$k]['src'] = wp_get_attachment_image_src($photo_id, 'stm-img-796-466')[0];
		$car_photos[$k]['thumb'] = wp_get_attachment_image_src($photo_id, 'stm-img-796-466')[0];
	}

	return $car_photos;
}

function formPhotoSwipeGallery($gallery){
	foreach ($gallery as $img) {
		$photos[] = wp_get_attachment_image_src($img, 'stm-img-796-466');
	}

	$photos = array_map(function($photo){
		return [
			'src' => $photo[0],
			'thumbnail' => $photo[0],
			'w' => $photo[1],
			'h' => $photo[2]
		];
	}, $photos);

	return $photos;
}

function get_all_packages(){

	return [
		'single_plan' => (int)get_theme_mod('single_plan'),
		'featured_plan' =>  (int)get_theme_mod('featured_plan'),
		'month_plan' => (int)get_theme_mod('month_plan'),
		'premium_plan' => (int)get_theme_mod('premium_plan'),
	];
}

function get_simple_color($ext_color){
	$terms = get_terms(array('taxonomy'=>'simple-color', 'hide_empty' => false));
//	if(is_wp_error($terms) || empty($terms)){
//
//	}
	$simple = wp_list_pluck($terms, 'name');
	foreach($simple as $color) {
		if (preg_match('/\b(\w*' . $color . '\w*)\b/', $ext_color)){
			return $color;
		}
	}
}

function get_random_digit(){
	return  rand(1, 1000);
}
