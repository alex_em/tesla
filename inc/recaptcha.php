<?php

/**
 * @param string $recaptchaToken
 *
 * @return array $response
 */
function recaptchaValidation(string $recaptchaToken): array
{
	$response = [];

	if (!$recaptchaToken) {
		$response['error'] = recaptcha_error_message();
		return $response;
	}

	$secretKey = get_theme_mod('recaptcha_secret_key');
	$url = 'https://www.google.com/recaptcha/api/siteverify';
	$body = [
		'secret' => $secretKey,
		'response' => $recaptchaToken,
		'remoteip' => $_SERVER["REMOTE_ADDR"],
	];
	$request = wp_remote_post($url, [
		'body' => $body,
	]);

	$response_body = wp_remote_retrieve_body($request);
	$response = json_decode($response_body, true);

	return $response;
}

function recaptcha_error_message(){
	return __('Recaptcha validation failed!', 'motors-child');
}