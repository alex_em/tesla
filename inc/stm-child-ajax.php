<?php

remove_action( 'wp_ajax_stm_ajax_add_a_car', 'stm_add_a_car' );
remove_action( 'wp_ajax_nopriv_stm_ajax_add_a_car', 'stm_add_a_car' );
add_action( 'wp_ajax_stm_ajax_add_a_car', 'stm_child_add_a_car' );
add_action( 'wp_ajax_nopriv_stm_ajax_add_a_car', 'stm_child_add_a_car' );

remove_action('wp_ajax_stm_ajax_add_a_car_media', 'stm_ajax_add_a_car_media');
remove_action('wp_ajax_nopriv_stm_ajax_add_a_car_media', 'stm_ajax_add_a_car_media');
add_action('wp_ajax_stm_ajax_add_a_car_media', 'stm_child_ajax_add_a_car_media');
add_action('wp_ajax_nopriv_stm_ajax_add_a_car_media', 'stm_child_ajax_add_a_car_media');

add_action('wp_ajax_tesla_add_plan_to_cart', 'tesla_add_plan_to_cart');
add_action('wp_ajax_nopriv_tesla_add_plan_to_cart', 'tesla_add_plan_to_cart');

add_action('wp_ajax_stm_pay_now', 'stm_pay_now');
add_action('wp_ajax_nopriv_stm_pay_now', 'stm_pay_now');
//ADD A CAR

function stm_child_add_a_car()
{
    check_ajax_referer( 'stm_add_a_car', 'security', false );

    $response = array();
    $first_step = array(); //needed fields
    $second_step = array(); //secondary fields
    $car_features = array(); //array of features car
    $videos = array(); /*videos links*/
    $notes = esc_html__( 'N/A', 'motors' );
    $registered = '';
    $vin = '';
    $history = array(
        'label' => '',
        'link' => ''
    );
    $location = array(
        'label' => '',
        'lat' => '',
        'lng' => '',
		'state' => '',
		'city' => ''
    );

    if ( !is_user_logged_in() ) {
        $response['message'] = esc_html__( 'Please, log in', 'motors' );
        wp_send_json( $response );
    } else {
        $user = stm_get_user_custom_fields( '' );
        $restrictions = stm_get_post_limits( $user['user_id'] );
    }


    $response['message'] = '';
    $error = false;

    $demo = stm_is_site_demo_mode();
    if ( $demo ) {
        $error = true;
        $response['message'] = esc_html__( 'Site is on demo mode', 'motors' );
    }

    $update = false;
    if ( !empty( $_POST['stm_current_car_id'] ) ) {
        $post_id = intval( $_POST['stm_current_car_id'] );
        $car_user = get_post_meta( $post_id, 'stm_car_user', true );
        $update = true;

        /*Check if current user edits his car*/
        if ( intval( $car_user ) != intval( $user['user_id'] ) ) {
            wp_die();
        }
    }

    /*Get first step*/

    if ( !empty( $_POST['stm_f_s'] ) ) {
        foreach ( $_POST['stm_f_s'] as $post_key => $post_value ) {
	        $postKey = str_replace( "_pre_", "-", $post_key );
	        if ( $_POST['stm_f_s'][$post_key] != "" ) {
                $first_step[sanitize_title( $postKey )] = sanitize_title( $_POST['stm_f_s'][$post_key] );
            } else {
                $error = true;
                $response['message'] = esc_html__( 'Enter required fields', 'motors' );
            }
            $arr[] = $post_key;
        }
    }

    if ( empty( $first_step )  ) {
        $error = true;
        $response['message'] = esc_html__( 'Enter required fields', 'motors' );
    }

    if ( !empty( $_POST['required'] ) ) {
        foreach ( $_POST['required'] as $post_key => $post_value ) {

            if ( $_POST['required'][$post_key] != "" ) {
                $_POST[sanitize_title( $post_key )] = sanitize_title( $_POST['required'][$post_key] );
            } else {
                $error = true;
                $response['message'] = esc_html__( 'Enter required fields', 'motors' );
            }
        }
    }


    /*Getting second step*/
    foreach ( $_POST as $second_step_key => $second_step_value ) {
        if ( strpos( $second_step_key, 'stm_s_s_' ) !== false ) {
            $original_key = str_replace( 'stm_s_s_', '', $second_step_key );
            $second_step[sanitize_title( $original_key )] = sanitize_text_field( $_POST[$second_step_key] );
        }
    }
    /*Getting car features*/
    if ( !empty( $_POST['stm_car_features_labels'] ) ) {
        foreach ( $_POST['stm_car_features_labels'] as $car_feature ) {
            $car_features[] = esc_attr( $car_feature );
        }
    }

    /*Videos*/
    if ( !empty( $_POST['stm_video'] ) ) {
        foreach ( $_POST['stm_video'] as $video ) {

            if ( ( strpos( $video, 'youtu' ) ) > 0 ) {
                $is_youtube = array();
                parse_str( parse_url( $video, PHP_URL_QUERY ), $is_youtube );
                if ( !empty( $is_youtube['v'] ) ) {
                    $video = 'https://www.youtube.com/embed/' . $is_youtube['v'];
                }
            }

            $videos[] = esc_url( $video );
            $videos = array_filter( $videos );
        }
    }

    /*Note*/
    if ( !empty( $_POST['stm_seller_notes'] ) ) {
        $notes = $_POST['stm_seller_notes'];
    }

    /*Registration date*/
    if ( !empty( $_POST['stm_registered'] ) ) {
        $registered = sanitize_text_field( $_POST['stm_registered'] );
    }

    /*Vin*/
    if ( !empty( $_POST['stm_vin'] ) ) {
        $vin = sanitize_text_field( $_POST['stm_vin'] );
    }

    /*History*/
    if ( !empty( $_POST['stm_history_label'] ) ) {
        $history['label'] = sanitize_text_field( $_POST['stm_history_label'] );
    }

    if ( !empty( $_POST['stm_history_link'] ) ) {
        $history['link'] = sanitize_text_field( $_POST['stm_history_link'] );
    }

    /*Location*/
    if ( !empty( $_POST['stm_location_text'] ) ) {
        $location['label'] = sanitize_text_field( $_POST['stm_location_text'] );
    }

    if ( !empty( $_POST['stm_lat'] ) ) {
        $location['lat'] = sanitize_text_field( $_POST['stm_lat'] );
    }

    if ( !empty( $_POST['stm_lng'] ) ) {
        $location['lng'] = sanitize_text_field( $_POST['stm_lng'] );
    }

	if ( !empty( $_POST['stm_state'] ) ) {
		$location['state'] = sanitize_text_field( $_POST['stm_state'] );
	}

	if ( !empty( $_POST['stm_city'] ) ) {
		$location['city'] = sanitize_text_field( $_POST['stm_city'] );
	}

    if ( empty( $_POST['stm_car_price'] ) ) {
    	$response['price_error'] = true;
        $error = true;
        //$response['message'] = esc_html__( 'Please add car price', 'motors' );
        $price = '';
        $normalPrice = '';

    } else {
        $normalPrice = abs( intval( str_replace(",", "", $_POST['stm_car_price'] ) ) ); //custom function to strip dynamic comma out of add car price
        $price = ( function_exists( 'stm_convert_to_normal_price' ) ) ? stm_convert_to_normal_price( $normalPrice ) : $normalPrice;
    }

    if ( isset( $_POST['car_price_form_label'] ) && !empty( $_POST['car_price_form_label'] ) ) {

        if ( empty( $_POST['stm_car_price'] ) ) {
            $error = false;
            unset( $response['message'] );
        }

        $location['car_price_form_label'] = sanitize_text_field( $_POST['car_price_form_label'] );
    } else {
        $location['car_price_form_label'] = '';
    }

    if ( isset( $_POST['stm_car_sale_price'] ) ) {
        $salePrice = abs( sanitize_text_field( $_POST['stm_car_sale_price'] ) );
        $location['stm_car_sale_price'] = ( function_exists( 'stm_convert_to_normal_price' ) ) ? stm_convert_to_normal_price( $salePrice ) : $salePrice;
    }

    $generic_title = '';
    if ( !empty( $_POST['stm_car_main_title'] ) ) {
        $generic_title = sanitize_text_field( $_POST['stm_car_main_title'] );
    }

    $motors_gdpr_agree = ( isset( $_POST['motors-gdpr-agree'] ) && !empty( $_POST['motors-gdpr-agree'] ) ) ? $_POST['motors-gdpr-agree'] : false;

    if ( $motors_gdpr_agree && $motors_gdpr_agree == 'not_agree' ) {
        $error = true;
        $gdpr = get_option( 'stm_gdpr_compliance', '' );
        $ppLink = ( $gdpr['stmgdpr_privacy'][0]['privacy_page'] != 0 ) ? get_the_permalink( $gdpr['stmgdpr_privacy'][0]['privacy_page'] ) : '';
        $ppLinkText = ( !empty( $gdpr ) && !empty( $gdpr['stmgdpr_privacy'][0]['link_text'] ) ) ? $gdpr['stmgdpr_privacy'][0]['link_text'] : '';
        $mess = sprintf( __( "Providing consent to our <a href='%s'>%s</a> is necessary in order to use our services and products.", 'motors' ), $ppLink, $ppLinkText );


        $response['html'] = 'html';
        $response['message'] = $mess;
    }

    /*Generating post*/
    if ( !$error ) {

        if ( $restrictions['premoderation'] ) {
            $status = 'pending';
            $user = stm_get_user_custom_fields( '' );
        } else {
            $status = 'publish';
        }

        if ( $_POST['btn-type'] == 'pay' ) {
            $status = 'pending';
        }

        if($_POST['status']){
            $status = esc_html($_POST['status']);
        }

        $post_data = array(
            'post_type' => stm_listings_post_type(),
            'post_title' => '',
            'post_content' => $notes,
            'post_status' => $status,
        );

        foreach ( $first_step as $taxonomy => $title_part ) {
            $term = get_term_by( 'slug', $title_part, $taxonomy );
            $post_data['post_title'] .= $term->name . ' ';
        }

        if ( !empty( $generic_title ) ) {
            $post_data['post_title'] = $generic_title;
        }


        if ( !$update ) {
            $post_id = wp_insert_post( apply_filters( 'stm_listing_save_post_data', $post_data ), true );
        }

        if ( !is_wp_error( $post_id ) ) {

            if ( $update ) {
                $post_data_update = array(
                    'ID' => $post_id,
                    'post_content' => $notes,
                    'post_status' => $status,
                );

                if ( !empty( $generic_title ) ) {
                    $post_data_update['post_title'] = $generic_title;
                }

                wp_update_post( apply_filters( 'stm_listing_save_post_data', $post_data_update ) );

            }

            $meta = array(
                'stock_number' => $post_id,
                'stm_car_user' => $user['user_id'],
                'price' => $price,
                'stm_genuine_price' => $price,
                'title' => 'hide',
                'breadcrumbs' => 'show',
            );

            $terms = array();

            /*Set categories*/
            foreach ( $first_step as $tax => $term ) {
                $tax_info = stm_get_all_by_slug( $tax );
                if ( !empty( $tax_info['numeric'] ) and $tax_info['numeric'] ) {
                    $meta[$tax] = abs( sanitize_title( $term ) );
                } else {
                    $terms[$tax] = $term;
                    $meta[$tax] = sanitize_title( $term );
                }
            }

            /*Set categories*/
            foreach ( $second_step as $tax => $term ) {

                $term = apply_filters( 'stm_change_value', $term );

                if ( !empty( $tax ) ) {
                    $tax_info = stm_get_all_by_slug( $tax );
                    if ( !empty( $tax_info['numeric'] ) and $tax_info['numeric'] ) {
                        update_post_meta( $post_id, $tax, $term );
                        $meta[$tax] = $term;
                    } else {
                        $terms[$tax] = $term;
                        $meta[$tax] = $term;
                    }
                }
            }

            if ( !empty( $videos ) ) {
                $meta['gallery_video'] = $videos[0];

                if ( count( $videos ) > 1 ) {
                    array_shift( $videos );
                    $meta['gallery_videos'] = array_filter( array_unique( $videos ) );
                }
            }

            $meta['vin_number'] = $vin;
            $meta['registration_date'] = $registered;
            $meta['history'] = $history['label'];
            $meta['history_link'] = $history['link'];
            $meta['stm_car_location'] = $location['label'];
            $meta['stm_lat_car_admin'] = $location['lat'];
            $meta['stm_lng_car_admin'] = $location['lng'];
            $meta['stm_lng_car_admin'] = $location['lng'];
            $meta['stm_state_car_admin'] = $location['state'];
            $meta['stm_city_car_admin'] = $location['city'];
            $meta['additional_features'] = implode( ',', $car_features );
            $terms['stm_additional_features'] = $car_features;

            update_post_meta( $post_id, 'price', $price );
            update_post_meta( $post_id, 'stm_genuine_price', $price );
            update_post_meta( $post_id, 'motors_gdpr_agree', get_the_date( 'd-m-Y', $post_id ) );

			$show_user_name = $_POST['show_user_name'] ? $_POST['show_user_name'] : '';
			$show_user_phone = $_POST['user_phone'] ? $_POST['user_phone'] : '';

			if($show_user_phone) {
				$calls_ok = $_POST['calls-ok'] ? $_POST['calls-ok'] : '';
				$texts_ok = $_POST['texts-ok'] ? $_POST['texts-ok'] : '';
				update_post_meta($post_id, 'calls_ok', $calls_ok);
				update_post_meta($post_id, 'texts_ok', $texts_ok);
			}

			update_post_meta($post_id, 'show_user_name', $show_user_name);
			update_post_meta($post_id, 'show_user_phone', $show_user_phone);

            if ( isset( $location['car_price_form_label'] ) ) {
                $meta['car_price_form_label'] = $location['car_price_form_label'];
            }

            if ( isset( $location['stm_car_sale_price'] ) && !empty( $location['stm_car_sale_price'] ) ) {
                $meta['sale_price'] = $location['stm_car_sale_price'];
                $meta['stm_genuine_price'] = $location['stm_car_sale_price'];
            } else {
                $meta['sale_price'] = '';
            }

            foreach ( apply_filters( 'stm_listing_save_post_meta', $meta, $post_id, $update ) as $key => $value ) {
                update_post_meta( $post_id, $key, $value );
            }

            foreach ( apply_filters( 'stm_listing_save_post_terms', $terms, $post_id, $update ) as $tax => $term ) {
                wp_delete_object_term_relationships( $post_id, $tax );
                wp_add_object_terms( $post_id, $term, $tax );

                update_post_meta( $post_id, $tax, sanitize_title( $term ) );
            }

            update_post_meta( $post_id, 'title', 'hide' );
            update_post_meta( $post_id, 'breadcrumbs', 'show' );

            $response['post_id'] = $post_id;
            $response['redirect_type'] = sanitize_text_field( $_POST['btn-type'] );
            if ( ( $update ) ) {
                $response['message'] = esc_html__( 'Car Updated, uploading photos', 'motors' );
            } else {
                $response['message'] = esc_html__( 'Car Added, uploading photos', 'motors' );
            }

	        if ( !$update ) {
                $title_from = get_theme_mod( 'listing_directory_title_frontend', '' );
                if ( !empty( $title_from ) ) {
                    wp_update_post( array( 'ID' => $post_id, 'post_title' => stm_generate_title_from_slugs( $post_id ) ) );
                }
            }

            do_action( 'stm_after_listing_saved', $post_id, $response, $update );

        } else {
            $response['message'] = $post_id->get_error_message();
        }
    }

    wp_send_json( apply_filters( 'stm_filter_add_a_car', $response ) );
}

function stm_child_ajax_add_a_car_media()
{
	if (stm_is_site_demo_mode()) {
		wp_send_json(array('message' => esc_html__('Site is on demo mode', 'stm_vehicles_listing')));
		exit;
	}

	$redirectType = (isset($_POST['redirect_type'])) ? $_POST['redirect_type'] : '';
	$post_id = intval($_POST['post_id']);
	if (!$post_id) {
		/*No id passed from first ajax Call?*/
		wp_send_json(array('message' => esc_html__('Some error occurred, try again later', 'stm_vehicles_listing')));
		exit;
	}

	$user_id = get_current_user_id();
	$limits = stm_get_post_limits($user_id);

	$updating = !empty($_POST['stm_edit']) and $_POST['stm_edit'] == 'update';

	if (intval(get_post_meta($post_id, 'stm_car_user', true)) != intval($user_id)) {
		/*User tries to add info to another car*/
		wp_send_json(array('message' => esc_html__('You are trying to add car to another car user, or your session has expired, please sign in first', 'stm_vehicles_listing')));
		exit;
	}


	$attachments_ids = array();
	foreach ($_POST as $get_media_keys => $get_media_values) {
		if (strpos($get_media_keys, 'media_position_') !== false) {
			$attachments_ids[str_replace('media_position_', '', $get_media_keys)] = intval($get_media_values);
		}
	}

	$error = false;
	$response = array(
		'message' => '',
		'post' => $post_id,
		'errors' => array(),
	);

	$files_approved = array();

	if (!empty($_FILES)) {

		$max_file_size = apply_filters('stm_listing_media_upload_size', 1024 * 4000); /*4mb is highest media upload here*/

		$max_uploads = intval($limits['images']) - count($attachments_ids);

		if (count($_FILES['files']['name']) > $max_uploads) {
			$error = true;
			$response['message'] = sprintf(esc_html__('Sorry, you can upload only %d images per add', 'stm_vehicles_listing'), $max_uploads);
		} else {
			// Check if user is trying to upload more than the allowed number of images for the current post
			foreach ($_FILES['files']['name'] as $f => $name) {
				if (count($files_approved) == $max_uploads) {
					break;
				} elseif ($_FILES['files']['error'][$f] != UPLOAD_ERR_OK) {
					$error = true;
				} else {
					// Check if image size is larger than the allowed file size

					// Check if the file being uploaded is in the allowed file types
					$check_image = @getimagesize($_FILES['files']['tmp_name'][$f]);
					if ($_FILES['files']['size'][$f] > $max_file_size) {
						$response['message'] = esc_html__('Sorry, image is too large', 'stm_vehicles_listing') . ': ' . $name;
						$error = true;
					} elseif (empty($check_image)) {
						$response['message'] = esc_html__('Sorry, image has invalid format', 'stm_vehicles_listing') . ': ' . $name;
						$error = true;
					} else {
						$tmp_name = $_FILES['files']['tmp_name'][ $f ];
						$error = $_FILES['files']['error'][ $f ];
						$type = $_FILES['files']['type'][ $f ];
						$files_approved[$f] = compact('name', 'tmp_name', 'type', 'error');
					}
				}
			}
		}
	}

	if ($error) {
		if (!$updating) {
			wp_delete_post($post_id, true);
		}
		wp_send_json($response);
		exit;
	}

	require_once(ABSPATH . 'wp-admin/includes/image.php');

	foreach ($files_approved as $f => $file) {
		$uploaded = wp_handle_upload($file, array('action' => 'stm_ajax_add_a_car_media'));

		if ($uploaded['error']) {
			$response['errors'][ $file['name'] ] = $uploaded;
			continue;
		}

		$filetype = wp_check_filetype(basename($uploaded['file']), null);

		// Insert attachment to the database
		$attach_id = wp_insert_attachment(array(
			'guid' => $uploaded['url'],
			'post_mime_type' => $filetype['type'],
			'post_title' => preg_replace('/\.[^.]+$/', '', basename($uploaded['file'])),
			'post_content' => '',
			'post_status' => 'inherit',
		), $uploaded['file'], $post_id);

		// Generate meta data
		//wp_update_attachment_metadata($attach_id, wp_generate_attachment_metadata($attach_id, $uploaded['file']));

		$attachments_ids[$f] = $attach_id;
	}

	$attachments_ids = $_POST['gallery'] ? $_POST['gallery'] : [];
	$current_attachments = get_post_meta($post_id, 'gallery', true);

	foreach ($current_attachments as $current_attachment) {
	    delete_post_meta($current_attachment, 'attachment_to_delete');
    }

	$delete_attachments = array_diff($current_attachments, $attachments_ids);
	foreach ($delete_attachments as $delete_attachment) {
		stm_delete_media(intval($delete_attachment));
	}

	//ksort($attachments_ids);
	if (!empty($attachments_ids)) {
		update_post_meta($post_id, '_thumbnail_id', reset($attachments_ids));
		array_shift($attachments_ids);
	} else {
        delete_post_meta($post_id, '_thumbnail_id');
    }

	update_post_meta($post_id, 'gallery', $attachments_ids);

	do_action( 'stm_after_listing_gallery_saved', $post_id, $attachments_ids );

	if ($updating) {
		$response['message'] = esc_html__('Car updated, redirecting to your account profile', 'stm_vehicles_listing');

		$to = get_bloginfo('admin_email');

		$args = array (
			'user_id' => $user_id,
			'car_id' => $post_id
		);
		$subject = generateSubjectView('update_a_car', $args);
		$body = generateTemplateView('update_a_car', $args);

		if($redirectType == 'edit-ppl') {
			$args = array (
				'user_id' => $user_id,
				'car_id' => $post_id,
				'revision_link' => getRevisionLink($post_id)
			);
			$subject = generateSubjectView('update_a_car_ppl', $args);
			$body = generateTemplateView('update_a_car_ppl', $args);
		}

	} else {
		$response['message'] = esc_html__('Car added, redirecting to your account profile', 'stm_vehicles_listing');

		$to = get_bloginfo('admin_email');
		$args = array (
			'user_id' => $user_id,
			'car_id' => $post_id
		);
		$subject = generateSubjectView('add_a_car', $args);
		$body = generateTemplateView('add_a_car', $args);
	}

	add_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');
	if(apply_filters('stm_listings_notify_updated', true)) {
		wp_mail($to, $subject, apply_filters('stm_listing_saved_email_body', $body, $post_id, $updating));
	}
	remove_filter('wp_mail_content_type', 'stm_set_html_content_type_mail');

	$response['success'] = true;

	$checkoutUrl = '';

	if(class_exists("WooCommerce") && get_theme_mod('dealer_pay_per_listing', false) && !$updating && !empty($redirectType) && $redirectType == 'pay') {
		$perPayListingPrice = get_theme_mod('pay_per_listing_price', 0);
		update_post_meta($post_id, '_price', $perPayListingPrice);
		update_post_meta($post_id, 'pay_per_listing', 'pay');

		$checkoutUrl =  wc_get_checkout_url() . '?add-to-cart=' . $post_id;
	}


	$response['url'] = (!empty($redirectType) && $redirectType == 'pay') ? $checkoutUrl : esc_url(get_author_posts_url($user_id));
	if(!empty($redirectType) && $redirectType == 'pay' && !$updating) {
		$response['message'] = esc_html__('Car added, redirecting to checkout', 'stm_vehicles_listing');
	}

    $is_user_have_premium_subs = is_user_have_premium_plan();

	$status = get_post_meta($post_id, 'paid', true);

	$all_packages_page = all_packages_page();

	$response['message'] = esc_html__('Car added, redirecting to .....', 'stm_vehicles_listing');
	$response['url'] = get_home_url() .'/'. $all_packages_page;

    if(  $is_user_have_premium_subs  ){

        $restrictions = stm_get_post_limits($user_id);

        update_post_meta($post_id, 'status', 'paid');
        $response['message'] = esc_html__('Car added, redirecting to your profile', 'stm_vehicles_listing');
        $response['url'] = esc_url(get_author_posts_url($user_id));

        if($updating && $status == 'not_paid' || $restrictions['posts'] < 1 ){
            $response['listing_id'] = $post_id;
            update_post_meta($post_id, 'status', 'not_paid');
            update_user_meta($user_id, 'not_paid', true);
            $response['url'] = get_home_url() .'/'. $all_packages_page;
        }



    }else{
        update_user_meta($user_id, 'can_buy', true);
        update_post_meta($post_id, 'status', 'not_paid');
        $response['listing_id'] = $post_id;
    }
	$user_meta=get_userdata($user_id);

	$user_roles=$user_meta->roles;
	if(in_array('administrator', $user_roles)){
		update_post_meta($post_id, 'status', 'paid');
		$response['message'] = esc_html__('Car added, redirecting to your profile', 'stm_vehicles_listing');
		$response['url'] = esc_url(get_author_posts_url($user_id));
	}

	wp_send_json(apply_filters('stm_filter_add_car_media', $response));
	exit;
}

function tesla_add_plan_to_cart(){
    $response = [];
    $product_id = $_POST['product_id'];

    $user_id = get_current_user_id();
    if($user_id){
        $can_user_buy = get_user_meta(get_current_user_id(), 'can_buy', true);
        $packages = get_all_packages();

        /*user can not buy single plan if he didnt add car before*/
        if(!$can_user_buy && $packages['single_plan'] === $product_id){
            $product_id = '';
        }

        /*user can not buy premium if he already has one*/
        if(is_user_have_premium_plan() && in_array($product_id, [$packages['month_plan'], $packages['premium_plan']] ) ){
            $product_id = '';
        }
    }else{
        $product_id = '';
    }

    WC()->cart->empty_cart();

    WC()->cart->add_to_cart( $product_id );

    $response['url'] = wc_get_cart_url();

    wp_send_json($response);
}

function stm_pay_now(){
    $response = [];
    $car_id = $_POST['product_id'];

    WC()->cart->empty_cart();

    $response['url'] = get_home_url() . '/' . all_packages_page();
    $response['listing_id'] = $car_id;

    wp_send_json($response);
}






