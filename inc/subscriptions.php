<?php

$GLOBALS['wp_filter'];

add_action( "init", "delete_functions" );
function delete_functions(){
   remove_action( "subscriptio_status_changed", "stm_updateListingsStatus",100 );
}

//user can have only one active sub
function is_user_have_premium_plan(){

	$user_id = get_current_user_id();
	$month_plan = (int)get_theme_mod('month_plan');
	$year_plan = (int)get_theme_mod('premium_plan');

	$plans_ids = [$month_plan, $year_plan];
	$subscription = stm_user_active_subscriptions(false, $user_id);

	if($subscription['product_id'] && in_array($subscription['product_id'], $plans_ids)){
		return true;
	}

	return false;
}