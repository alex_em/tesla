<?php

if (function_exists('vc_map')) {
    add_action('init', 'register_child_elements');
}

function register_child_elements(){


    $plan_args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'post_status' => 'publish'
    );

    $products = new WP_Query($plan_args);
    $products_array = array(__('Choose plan', 'motors') => '');
    if ($products->have_posts()) {
        while ($products->have_posts()) {
            $products->the_post();
            $title = get_the_title();
            $id = get_the_ID();
            $products_array[$title] = $id;
        }
    }

    $stm_pt_params = array();

    for ($i = 1; $i <= 4; $i++) {
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Title', 'motors'),
            'param_name' => 'pt_' . $i . '_title',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Subtitle', 'motors'),
            'param_name' => 'pt_' . $i . '_subtitle',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );
        $stm_pt_params[] = array(
            'type' => 'textfield',
            'heading' => __('Plan Price', 'motors'),
            'param_name' => 'pt_' . $i . '_price',
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );


        $stm_pt_params[] = array(
            'type' => 'param_group',
            'heading' => __('Features', 'motors'),
            'param_name' => 'pt_' . $i . '_features',
            'value' => urlencode(json_encode(array(
                array(
                    'label' => __('Value', 'motors'),
                    'value' => '',
                ),
                array(
                    'label' => __('Label', 'motors'),
                    'value' => '',
                )
            ))),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Value', 'motors'),
                    'param_name' => 'pt_' . $i . '_feature_title',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Label', 'motors'),
                    'param_name' => 'pt_' . $i . '_feature_text',
                    'admin_label' => true,
                )
            ),
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );

        $stm_pt_params[] = array(
            'type' => 'dropdown',
            'heading' => __('Plan add to cart (Plan ID)', 'motors'),
            'param_name' => 'pt_' . $i . '_add_to_cart',
            'value' => $products_array,
            'group' => sprintf(__("Table %s", 'motors'), $i)
        );
    }

    vc_map(array(
        'name' => __('STM Tesla Pricing Tables', 'motors'),
        'base' => 'stm_tesla_pricing',
        'category' => __('', 'motors'),
        'params' => $stm_pt_params
    ));

    vc_map(array(
        'name' => __('Algolia Search Filter', 'motors-child'),
        'base' => 'stm_algolia_filter',
        'category' => __('STM', 'motors'),
        'params' => array(
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'motors'),
                'param_name' => 'css',
                'group' => __('Design options', 'motors')
            )
        )
    ));
    vc_map(array(
        'name' => __('Register Form', 'motors-child'),
        'base' => 'stm_register_form',
        'icon' => 'stm_add_a_car',
        'params' => array(
            array(
                'type' => 'vc_link',
                'heading' => __('Link', 'motors'),
                'param_name' => 'link'
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose redirect page', 'motors-child' ),
                'param_name'  => 'redirect_page',
                'description' => 'Note* if field is empty it will automatically redirect to author page after successful sign up.',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose Sing-In page', 'motors-child' ),
                'param_name'  => 'login_redirect_page',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
        ),
    ));

    vc_map(array(
        'name' => __('Login Form', 'motors-child'),
        'base' => 'stm_login_form',
        'icon' => 'stm_add_a_car',
        'params' => array(
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose redirect page', 'motors-child' ),
                'param_name'  => 'redirect_page',
                'description' => 'Note* if field is empty it will automatically redirect to author page after successful sign in.',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
            array(
                'type'        => 'autocomplete',
                'heading'     => esc_html__( 'Choose Sing-Up page', 'motors-child' ),
                'param_name'  => 'login_redirect_page',
                'settings'    => array(
                    'multiple'      => false,
                    'sortable'      => false,
                    'min_length'    => 1,
                    'no_hide'       => false,
                    'groups'        => true,
                    'unique_values' => true,
                )
            ),
        ),
    ));

}
add_filter( 'vc_autocomplete_stm_login_form_login_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_redirect_page_render', 'all_post_suggester_render', 10, 1 );

add_filter( 'vc_autocomplete_stm_register_form_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );
add_filter( 'vc_autocomplete_stm_register_form_login_redirect_page_callback', 'posts_auto_complete_suggetster', 10, 1 );

add_filter( 'vc_autocomplete_stm_register_form_redirect_page_render', 'all_post_suggester_render', 10, 1 );
add_filter( 'vc_autocomplete_stm_register_form_login_redirect_page_render', 'all_post_suggester_render', 10, 1 );
add_filter( 'vc_autocomplete_stm_login_form_login_redirect_page_render', 'all_post_suggester_render', 10, 1 );

function posts_auto_complete_suggetster( $query) {
    global $wpdb;
    $post_id = (int) $query;
    $post_results = $wpdb->get_results( $wpdb->prepare( "SELECT a.ID AS id, a.post_title AS title FROM {$wpdb->posts} AS a
WHERE a.post_type = 'page'  AND a.post_status != 'trash' AND ( a.ID = '%d' OR a.post_title LIKE '%%%s%%' )", $post_id > 0 ? $post_id : - 1, stripslashes( $query ), stripslashes( $query ) ), ARRAY_A );
    $results = array();
    if ( is_array( $post_results ) && ! empty( $post_results ) ) {
        foreach ( $post_results as $value ) {
            $data = array();
            $data['value'] = $value['id'];
            $data['label'] = $value['title'];
            $results[] = $data;
        }
    }
    return $results;
}

function all_post_suggester_render( $query ) {
    $query = trim( $query['value'] );

    // get value from requested
    if ( ! empty( $query ) ) {
        $post_object = get_post( (int) $query );
        if ( is_object( $post_object ) ) {
            $post_title = $post_object->post_title;
            $post_id = $post_object->ID;
            $data = array();
            $data['value'] = $post_id;
            $data['label'] = $post_title;
            return ! empty( $data ) ? $data : false;
        }
        return false;
    }
    return false;
}


if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_Stm_Tesla_Pricing extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Algolia_Filter extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Register_Form extends WPBakeryShortCode
    {
    }
    class WPBakeryShortCode_Stm_Login_Form extends WPBakeryShortCode
    {
    }
}
