<?php

add_action('woocommerce_order_status_completed', 'change_listing_status');
add_action('woocommerce_order_status_processing', 'change_listing_status');
add_action('woocommerce_thankyou', 'redirect_after_purchase');
add_filter( 'woocommerce_checkout_fields', 'override_checkout_fields' );

function redirect_after_purchase($order_id)
{
    $order = wc_get_order($order_id);
    if (!$order->has_status('failed')) {
        wp_redirect(stm_get_author_link(get_current_user_id()));
        exit;
    }
}

add_filter('woocommerce_add_cart_item', function ($item) {
    if ($listing_id = $_COOKIE['listing_id']) {
        $item['listing_id'] = $listing_id;
    }
    return $item;
});

add_action('woocommerce_checkout_create_order_line_item', function ($item, $cart_item_key, $values) {
    if (isset($values['listing_id'])) {
        $item->update_meta_data('listing_id', $values['listing_id']);
    }
}, 10, 3);

function change_listing_status($order_id)
{
    $order = wc_get_order($order_id);
    $items = $order->get_items();
    $packages = get_all_packages();
    foreach ($items as $item) {
        $listing_id = (int)$item->get_meta('listing_id');

        if (!$listing_id) {
            return;
        }
        $product_id = $item->get_product_id();

        $user_id = get_post_meta($order_id, '_customer_user', true);

        foreach ($packages as $package => $id){
            if(!is_user_have_premium_plan() && $product_id == $id){
                $plan = $package;
            }
        }

        update_user_meta($user_id, 'current_plan', $plan);
        update_post_meta($listing_id, 'status', 'paid');

        wp_update_post([
            'ID' => $listing_id,
            'post_status' => 'pending'
        ]);

    }
}

function override_checkout_fields( $fields = array() ) {
	if(is_user_logged_in()){
		$user = wp_get_current_user();
		$user_id = $user->ID;
		$first_name = get_user_meta( $user_id, 'first_name', true );
		$last_name = get_user_meta( $user_id, 'last_name', true );
		$phone = get_user_meta( $user_id, 'stm_phone', true );

		$_POST['billing_first_name'] = $first_name ? $first_name : $_POST['billing_first_name'];
		$_POST['billing_last_name'] = $last_name ? $last_name :  $_POST['billing_last_name'];
		$_POST['billing_phone'] = $phone ? $phone : $_POST['billing_phone'];
	}

	return $fields;
}


