import Vue from 'vue';
import draggable from 'vuedraggable';
import VueTouch from 'vue-touch';

Vue.use(VueTouch, {name: 'v-touch'});

const confirmationDelete = $(
    '.stm-delete-confirmation-popup, .stm-delete-confirmation-overlay');
window.imageUploader = new Vue({
  el: '#image_uploader',
  data: {
    minPhotosLimit: 5,
    maxPhotosLimit: 20,
    files: '',
    featuredIndex: 0,
    isDraggable: false,
    lastUploadedIndex: 0,
    totalUploaded: 0,
    images: imageUploadSettings.images,
    previews: [],
    allowedExt: /(\.jpg|\.jpeg|\.png)$/i,
    a: '',
  },
  components: {
    draggable,
  },
  methods: {
    onMove(e) {
      return !e.related.classList.contains('image_uploader__item-preview');
    },

    handleSelects(event) {
      event.preventDefault();
      this.files = [];
      this.files = Array.prototype.slice.call(event.target.files).reverse();

      //give an opportunity to upload the same images
      event.target.value = '';

      const diff = this.totalUploaded + this.files.length - this.maxPhotosLimit;
      if (diff > 0) {
        for (let i = 0; i < diff; i++) {
          this.files.splice(this.files.length - i, 1);
        }
        alert(
            `Sorry, but some files will not be uploaded due to limit of ${this.maxPhotosLimit} photos`);
      }

      this.files.forEach((file, index) => {

        if (!this.allowedExt.exec(file.name)) {
          alert(`Sorry, invalid image format: ${file.name}`);
          return;
        }

        if (file.size > imageUploadSettings.size) {
          alert(`Sorry, image is too large: ${file.name}`);
          return;
        }

        if (this.previews[this.lastUploadedIndex]) {
          this.previews[this.lastUploadedIndex].preload = true;
          this.uploadImages(file, this.previews[this.lastUploadedIndex].index);
        }
      });

      this.files = [];
    },

    clickHandle(index) {
      if (index === this.triggerIndex) {
        this.$refs.inputUploader.click();
      }
    },
    makeThumbFromFile: function(imageFile) {
      return {url: URL.createObjectURL(imageFile)};
    },
    uploadImages(file, lastUploadedIndex) {
      const fd = new FormData();
      const blob = this.makeThumbFromFile(file)

      fd.append('action', 'custom_photo_upload');
      fd.append('lastUploadedIndex', lastUploadedIndex);
      fd.append('files[' + 1 + ']', file);
      fd.append('blob', blob.url)

      $.ajax({
        xhr: () => {
          var xhr = $.ajaxSettings.xhr();
          xhr.upload.addEventListener('progress', (evt) => {
            if (evt.lengthComputable) {
              const percentComplete = Math.ceil(evt.loaded / evt.total * 100);
              const item = this.getPreviewByOriginalIndex(lastUploadedIndex);
              item.progress = percentComplete;
            }
          }, false);
          return xhr;
        },
        target: '#stm_sell_a_car_form',
        type: 'POST',
        url: ajaxurl,
        data: fd,
        contentType: false,
        processData: false,
        context: this,
        beforeSend: () => {
          ++this.lastUploadedIndex;
        },
        error: (response, status, e) => {
          alert('Oops something went.');
        },
        success: (response) => {
          let item = this.getPreviewByOriginalIndex(response.index);
          item.src = blob.url;
          item.thumb = blob.url;
          item.preload = false;
          item.id = response.id;
          ++this.totalUploaded;
          if (this.totalUploaded === 1) {
            this.featuredIndex = this.previews[0].index;
          }
        },
      });
    },

    onTap(item) {
      let index = this.previews.indexOf(item);
      this.featuredIndex = this.previews[index].index;
    },

    deleteAll() {
      this.images = [];
      this.previews.forEach(el => {
        el.src = '';
      });
      this.lastUploadedIndex = 0;
      this.totalUploaded = 0;
      confirmationDelete.addClass('stm-disabled');
    },

    deleteSingle(index) {
      this.isDraggable = true;
      this.previews.splice(index, 1);
      this.previews.push(this.singleImage());
      this.lastUploadedIndex = this.lastUploadedIndex ?
          --this.lastUploadedIndex :
          0;
      this.totalUploaded = --this.totalUploaded;

      if (index === this.featuredIndex && index !== 0) {
        this.featuredIndex = 0;
      }

      this.isDraggable = false;
    },

    singleImage() {
      return {
        src: '',
        thumb: '',
        preload: false,
        index: this.previews[this.previews.length - 1]
            ? this.previews[this.previews.length - 1].index + 1
            : '',
        progress: 0,
      };
    },

    generatePreviews() {
      this.previews = [];
      for (let i = 0; i < this.maxPhotosLimit; i++) {
        let item = this.singleImage();
        if (this.images[i]) {
          const {src, thumb, id} = this.images[i];

          item.src = src
					item.thumb = thumb
					item.id = id
        }
        this.previews.push({
          ...item,
          index: i,
        });
      }
    },

    getPreviewByOriginalIndex(originalIndex) {
      return this.previews.find(el => {
        return el.index == originalIndex;
      });
    },

  },
  computed: {
    imageCount() {
      let limit;
      let wordCount
      if (this.previewsLength === this.maxPhotosLimit) {
        return 'Photo limit reached';
      }

      if (this.previewsLength < this.minPhotosLimit) {
        limit = this.minPhotosLimit - this.previewsLength;
      } else {
        limit = this.maxPhotosLimit - this.previewsLength;
      }

      if (limit === 1) {
        return 'We recommend adding ' + limit + ' more photo';
      }

      return 'We recommend adding ' + limit + ' more photos';
    },

    imagesLength() {
      return this.images.length;
    },

    previewsLength() {
      return this.previews.filter(el => el.src).length;
    },

    triggerIndex() {
      if (!this.previews[this.lastUploadedIndex]) {
        return;
      }
      if (this.previews[this.lastUploadedIndex].preload ||
          this.previews[this.lastUploadedIndex].src) {
        return ++this.lastUploadedIndex;
      }

      return this.lastUploadedIndex;
    },

    featuredImage() {
      return this.getPreviewByOriginalIndex(this.featuredIndex);
    },

  },

  created() {
    this.generatePreviews();
    this.lastUploadedIndex = this.previewsLength;
    this.totalUploaded = this.previewsLength;
  },
});

$(document).ready(function() {

  $('#delete-all').click(function() {
    confirmationDelete.removeClass('stm-disabled');
  });

  confirmationDelete.find('.actions a, .fa.fa-close').click(function(event) {
    event.preventDefault();
    if (event.target.id === 'delete') {
      imageUploader.deleteAll();
    } else {
      confirmationDelete.addClass('stm-disabled');
    }
  });

});
