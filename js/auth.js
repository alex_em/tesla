export function userRegister() {
	$('.stm-register-form form').submit(function (e) {
		e.preventDefault()
		grecaptcha.ready(() => {
			grecaptcha.execute(recaptcha_site_key, {action: 'homepage'}).then((token) => {
				$.ajax({
					type: 'POST',
					url: ajaxurl,
					dataType: 'json',
					context: this,
					data: $(this).serialize() + '&action=stm_custom_register' + '&recaptcha_token=' + token,
					beforeSend: function () {
						$(this).find('input').removeClass('form-error')
						$(this).find('.stm-listing-loader').addClass('visible')
						$('.stm-validation-message').empty()
					},
					success: function (data) {
						if (data.user_html) {
							var $user_html = $(data.user_html).appendTo('#stm_user_info')
							$('.stm-not-disabled, .stm-not-enabled').slideUp('fast', function () {
								$('#stm_user_info').slideDown('fast')
							})
							$('html, body').animate({scrollTop: $('.stm-form-checking-user').offset().top}, 'slow')

							$('.stm-form-checking-user button[type="submit"]').removeClass('disabled').addClass('enabled').html(
								'<i class="stm-service-icon-add_check"></i>' +
								'List Your Tesla: Select Your Package',
							)
						}

						if (data.restricted && data.restricted) {
							$('.btn-add-edit').remove()
						}

						$(this).find('.stm-listing-loader').removeClass('visible')
						for (var err in data.errors) {
							$(this).find('input[name=' + err + ']').addClass('form-error')
						}
						if (data.redirect_url) {
							window.location = data.redirect_url
						}

						if (data.message) {
							var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide()

							$(this).find('.stm-validation-message').append(message)
							message.slideDown('fast')
						}
					},
				})
			});
		});
	})
}

export function userLogin() {
	$('.lOffer-account-unit').mouseout(function () {
		$('.stm-login-form-unregistered').removeClass('working');
	});
	$('.stm-forgot-password a').on('click', function (e) {
		e.preventDefault();
		$('.stm_forgot_password_send').slideToggle();
		$('.stm_forgot_password_send input[type=text]').focus();
		$(this).toggleClass('active');
	})
	$(".stm-login-form-mobile-unregistered form,.stm-login-form form:not(.stm_password_recovery), .stm-login-form-unregistered form").submit(function (e) {
		e.preventDefault();
		if (!$(this).hasClass('stm_forgot_password_send')) {
			grecaptcha.ready(() => {
				grecaptcha.execute(recaptcha_site_key, {action: 'homepage'}).then((token) => {
					$.ajax({
						type: "POST",
						url: ajaxurl,
						dataType: 'json',
						context: this,
						data: $(this).serialize() + '&action=stm_custom_login' + '&recaptcha_token=' + token,
						beforeSend: function () {
							$(this).find('input').removeClass('form-error');
							$(this).find('.stm-listing-loader').addClass('visible');
							$('.stm-validation-message').empty();

							if ($(this).parent('.lOffer-account-unit').length > 0) {
								$('.stm-login-form-unregistered').addClass('working');
							}
						},
						success: function (data) {
							if ($(this).parent('.lOffer-account-unit').length > 0) {
								$('.stm-login-form-unregistered').addClass('working');
							}
							if (data.user_html) {
								var $user_html = $(data.user_html).appendTo('#stm_user_info');
								$('.stm-not-disabled, .stm-not-enabled').slideUp('fast', function () {
									$('#stm_user_info').slideDown('fast');
								});

								$("html, body").animate({scrollTop: $('.stm-form-checking-user').offset().top}, "slow");
								$('.stm-add-a-car-login-overlay,.stm-add-a-car-login').toggleClass('visiblity');

								$('.stm-form-checking-user button[type="submit"]').removeClass('disabled').addClass('enabled');
							}

							if (data.restricted && data.restricted) {
								$('.btn-add-edit').remove();
							}

							$(this).find('.stm-listing-loader').removeClass('visible');
							for (var err in data.errors) {
								$(this).find('input[name=' + err + ']').addClass('form-error');
							}

							if (data.message) {
								var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide();

								$(this).find('.stm-validation-message').append(message);
								message.slideDown('fast');
							}


							if (typeof (data.redirect_url) !== 'undefined') {
								window.location = data.redirect_url;
							}
						}
					});
				})
			})
		} else {
			/*Send passs*/
			grecaptcha.ready(() => {
				grecaptcha.execute(recaptcha_site_key, {action: 'homepage'}).then((token) => {
					$.ajax({
						type: "POST",
						url: ajaxurl,
						dataType: 'json',
						context: this,
						data: $(this).serialize() + '&action=stm_restore_password&security=' + restorePassword + '&recaptcha_token=' + token,
						beforeSend: function () {
							$(this).find('input').removeClass('form-error');
							$(this).find('.stm-listing-loader').addClass('visible');
							$('.stm-validation-message').empty();
						},
						success: function (data) {
							$(this).find('.stm-listing-loader').removeClass('visible');
							if (data.message) {
								var message = $('<div class="stm-message-ajax-validation heading-font">' + data.message + '</div>').hide();

								$(this).find('.stm-validation-message').append(message);
								message.slideDown('fast');
							}
							for (var err in data.errors) {
								$(this).find('input[name=' + err + ']').addClass('form-error');
							}
						}
					});
				})
			})
		}
	});

	$('.user_validated_field').on('hover', function () {
		$(this).removeClass('form-error');
	});

	$('input[name="stm_accept_terms"]').on('click', function () {
		if ($(this).is(':checked')) {
			$('.stm-login-register-form .stm-register-form form input[type="submit"]').removeAttr('disabled');
		} else {
			$('.stm-login-register-form .stm-register-form form input[type="submit"]').attr('disabled', '1');
		}
	});
}
