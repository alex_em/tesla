STMCascadingSelect.prototype.selector = function (slug) {
  if (this.relations[slug].selector) {
    return this.relations[slug].selector
  }

  return '[name="' + slug + '"]'
}

function arrayContainsAll (source, values) {
  for (var i in values) {
    if (source.indexOf(values[i]) === -1) {
      return false
    }
  }
  return true
}

STMCascadingSelect.prototype.selectbox = function (slug, config) {
  var parent = config.dependency

  var parent_arr = []
  if (parent) {
    for (var i in parent) {
      parent_arr.push(this.selector(parent[i]))
    }
  }

  return {
    selector: this.selector(slug),
    paramName: slug,
    requires: parent_arr ? parent_arr : null,
    requireAll: false,
    allowAll: config.allowAll,
    selected: $(this.selector(slug), this.ctx).data('selected'),
    source: function (request, response) {
      var selected = []
      for (var i in parent) {
        if (request[parent[i]]) {
          selected.push(request[parent[i]])
        }
      }

      var options = []

      $.each(config.options, function (i, option) {
        if ((config.allowAll && !selected) || arrayContainsAll(option.deps, selected)) {
          options.push(option)
        }
      })

      response(options)
    },
  }
}

STMCascadingSelect.prototype.selector = function (slug) {
  if (!this.relations[slug]) this.relations[slug] = []
  if (this.relations[slug].selector) {
    return this.relations[slug].selector
  }
  return '[name="' + slug + '"]'
}
