import { userRegister, userLogin } from './auth.js'

(function ($) {
  $(document).ready(function () {

    STMListings.stm_ajax_registration = userRegister
    STMListings.stm_ajax_login = userLogin

    var onResize = function () {
      if ($(window).width() < 768) {
        var liWrapper = $('.car-listing-top-part').find('.stm-listing-tabs li')

        liWrapper.click(function (e) {
          var ids = liWrapper.find('a').attr('href')
          setTimeout(function () {
            $('.tab-content').animatescroll({ scrollSpeed: 300, easing: 'easeOutSine' })
          }, 200)
        })
      }
    }

    $('.scrollToTop').click(function () {
      $('#header').animatescroll({ scrollSpeed: 800, easing: 'easeOutSine' })
    })

    function onScroll () {
      var scrollToTop = $('.scrollToTop')
      $(document).scrollTop() >= 800 ? scrollToTop.addClass('active') : scrollToTop.removeClass('active')
    }

    onResize()
    window.onresize = onResize
    window.onscroll = onScroll

    $('#tesla_pricing .select button, #upgrade_now').click(function (e) {
      e.preventDefault()
      var product_id = $(this).data('product-id')
      $.ajax({
        url: ajaxurl,
        type: 'POST',
        dataType: 'json',
        data: {
          action: 'tesla_add_plan_to_cart',
          product_id: product_id,
        },
        beforeSend: function () {
          $('#tesla_pricing .select button').prop('disabled', true)
        },
        success: function (data) {
          if (data.url) {
            window.location = data.url
          }
        },
      })
    })

    $('#pay_now').click(function (e) {
      e.preventDefault()
      var car_id = $(this).data('car-id')
      $.ajax({
        url: ajaxurl,
        type: 'POST',
        dataType: 'json',
        data: {
          action: 'stm_pay_now',
          product_id: car_id,
        },
        beforeSend: function () {
          $(this).prop('disabled', true)
        },
        success: function (data) {
          if (data.url) {
            $.cookie('listing_id', data.listing_id, { path: '/' })
            window.location = data.url
          }
        },
      })
    })


    function styckyEl(){
      var $el = $('#tesla-results');
      if ($(window).scrollTop() > 424 +  +$el.height() ){
        $el.addClass('fixed-filter-bar')
      }else{
        $el.removeClass('fixed-filter-bar')
      }
    }
    styckyEl()
    $(window).scroll(styckyEl);

    const $inputs = $('input[type=text] , input[type=tel] , input[type=email]  ');

    $(document).on('input change', 'input[type=text], input[type=tel] , input[type=email]', function(){
      checkInput($(this))
    })

    $inputs.each((index, el) => {
      checkInput($(el))
    } )

    function checkInput(input){
      if(input.val()) {
        input.addClass('active')
      }else {
        input.removeClass('active')
      }
    }
    $('#uniform-user_phone').click(function(event){
      $('.contact-preferences__sub-items').slideToggle();
      $('.user-phone__calls-ok, .user-phone__texts-ok').each(function () {
        $(this).find('span')
          .removeClass('checked')
          .find('input').prop('checked', false)
      })
    })

    var i = 0, timeOut = 0;
    var passInputType = $('.stm-show-password .user_validated_field');
    $('.stm-show-password .fa.fa-eye-slash').on('mousedown touchstart', function(e) {
      console.log(passInputType.attr('type'))
      if (passInputType.attr('type') === 'password') {
        console.log(123)
          passInputType.attr('type', 'text')
      }
      timeOut = setInterval(() => {
      }, 100);
    }).bind('mouseup mouseleave touchend', function() {
      if (passInputType.attr('type') === 'text') {
        passInputType.attr('type', 'password')
      }
      clearInterval(timeOut);
    });
  })
}(jQuery))
