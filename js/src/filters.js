const filters = [
	{
		title: 'Year',
		slug: 'year',
		type: 'checkboxes',
		attr: 'for_faceting.year',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Model',
		slug: 'model',
		attr: 'model',
		hasChild: false,
		sortBy: 'sortByAsc'
	},
	{
		title: 'Model Specific Trim',
		slug: 'model-trim',
		hasChild: false,
		attr: 'for_faceting.modelTrim',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Price',
		slug: 'price',
		hasChild: false,
		attr: 'price',
		type: 'range-input',
		min: 0,
		max: 10000000,
	},
	{
		title: 'Mileage',
		slug: 'mileage',
		hasChild: false,
		attr: 'mileage',
		sortBy: 'sortByAsc',
		type: 'numeric-menu'
	},
	{
		title: 'Exterior Color',
		slug: 'exterior-color',
		hasChild: true,
		attr: 'for_faceting.extColor',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Interior Color',
		slug: 'int-color',
		hasChild: false,
		attr: 'for_faceting.intColor',
		sortBy: 'sortByAsc'
	},
	{
		title: 'State',
		slug: 'state',
		hasChild: false,
		attr: 'state',
		sortBy: 'state'
	},
	{
		title: 'Autopilot Software',
		slug: 'auto-pilot-soft',
		hasChild: false,
		attr: 'for_faceting.autopilotSoftware',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Autopilot Hardware',
		slug: 'auto-pilot-hard',
		hasChild: false,
		attr: 'for_faceting.autopilotHardware',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Battery',
		slug: 'battery',
		hasChild: false,
		attr: 'for_faceting.autopilotHardware',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Drive',
		slug: 'drive',
		hasChild: false,
		attr: 'for_faceting.drive',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Performance',
		slug: 'performance',
		hasChild: false,
		attr: 'for_faceting.performance',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Seat Material',
		slug: 'seating',
		hasChild: false,
		attr: 'for_faceting.seating',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Dash',
		slug: 'dash',
		hasChild: false,
		attr: 'for_faceting.dash',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Wheels',
		slug: 'wheels',
		hasChild: false,
		attr: 'for_faceting.wheels',
		sortBy: 'sortByAsc'
	},
	{
		title: 'Seller Type',
		slug: 'seller-type',
		hasChild: false,
		attr: 'for_faceting.sellerType',
		sortBy: 'sortByAsc'
	},
]

export default filters
