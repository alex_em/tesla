const mileageOptions = [
	{label: 'All'},
	{label: '10,000 or less', end: 10000},
	{label: '20,000 or less', end: 20000},
	{label: '30,000 or less', end: 30000},
	{label: '40,000 or less', end: 40000},
	{label: '50,000 or less', end: 50000},
	{label: '60,000 or less', end: 60000},
	{label: '70,000 or less', end: 70000},
	{label: '80,000 or less', end: 80000},
	{label: '90,000 or less', end: 90000},
	{label: '100,000 or less', end: 100000},
	{label: '150,000 or less', end: 150000},
	{label: '200,000 or less', end: 200000},
	{label: '250,000 or less', end: 250000},
]

export default mileageOptions