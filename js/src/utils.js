const classNames = {
	accordion: {
		'ais-RefinementList-list' : 'stm-accordion-content-wrapper',
		'ais-RefinementList-item' : 'stm-single-unit'
	},
	color: {
		'ais-RefinementList-list' : 'stm-accordion-content-wrapper',
		'ais-RefinementList-item' : 'stm-single-unit color'
	},
	sortBy:{
		'ais-SortBy' : 'stm-select-sorting',
		'ais-SortBy-select' : 'select2-hidden-accessible'
	},
	grid:{
		'ais-Hits': 'row row-3 car-listing-row car-listing-modern-grid',
		'ais-Hits-list': 'stm-isotope-sorting',
		'ais-Hits-item': 'col-md-4 col-sm-4 col-xs-12 col-xxs-12 stm-isotope-listing-item all',
	},
	list:{
		'ais-Hits-list': 'stm-isotope-sorting',
		'ais-Hits-item': 'listing-list-loop stm-listing-directory-list-loop stm-isotope-listing-item all',
	},
	hierarchicalMenu: {
		'ais-HierarchicalMenu-list' : 'stm-accordion-content-wrapper',
		'ais-HierarchicalMenu-item' : 'stm-single-unit'
	}
}

export default classNames