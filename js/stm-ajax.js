jQuery(function ($) {
	$(document).ready(function ($) {
		const body = $("body")
		body.off("click", ".stm-show-number");
		body.on("click", '.stm-show-number', function () {
			const callLink = $('a.calls_ok')
			const textLink = $('a.texts_ok')
			let parent = $(this).parent();
			let phone_owner_id = $(this).attr("data-id");
			parent.find(".stm-show-number").text('').addClass('load_number');

			grecaptcha.ready(function () {
				grecaptcha.execute(recaptcha_site_key, {action: 'homepage'}).then(function (token) {
					$.ajax({
						url: ajaxurl,
						type: "GET",
						dataType: 'json',
						context: this,
						data: 'phone_owner_id=' + phone_owner_id + '&action=stm_ajax_get_seller_phone&security=' + sellerPhone +
							'&recaptcha-token=' + token,
						success: function (data) {
							if (!data.error && data.phone) {
								$('.span').addClass('hide')
								if(callLink.length) {
									callLink.attr('href', 'tel:' + data.phone).removeClass('hide')
								}

								if(textLink.length) {
									textLink.attr('href','sms:' + data.phone).removeClass('hide')
								}

								parent.find(".stm-show-number").hide();
								parent.find(".phone").html('<span style="font-size: 20px; ">' + data.phone + '</span>');
							} else {
								alert(data.error);
								parent.find(".stm-show-number").text('Show number').removeClass('load_number');
							}
						}
					});
				});
			});
		});


		body.on("click", ".calls_ok, .texts_ok", function(event){
			if ($(window).width() > 897 ) {
				event.preventDefault()
				return
			}
		})
	})
})
