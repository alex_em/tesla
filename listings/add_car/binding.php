<script type="text/javascript">

  jQuery(function ($) {

    var options = <?php echo json_encode( stm_data_binding_custom( true ) ); ?>

    $.each(options, function (slug, config) {
      config.selector = '#' + slug
    })

    $('.stm_add_car_form').each(function () {
      new STMCascadingSelect(this, options)
    })

  })
</script>
