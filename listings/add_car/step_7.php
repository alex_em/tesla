<?php
$user = get_userdata(get_current_user_id());
$name = $user->first_name ? $user->first_name : $user->user_login;
$user_phone = get_user_meta($user->ID, 'stm_phone', true);

if ($post_id = $_GET['item_id']) {
	$show_user_phone = get_post_meta($post_id, 'show_user_phone', true);
	$show_user_name = get_post_meta($post_id, 'show_user_name', true);
	$texts_ok = get_post_meta($post_id, 'texts_ok', true);
	$calls_ok = get_post_meta($post_id, 'calls_ok', true);
}

?>

<div class="stm-form-price-edit contact-preferences">
    <div class="stm-car-listing-data-single stm-border-top-unit ">
        <div class="title heading-font"><?php esc_html_e('Contact preferences', 'motors'); ?></div>
        <span class="step_number step_number_5 heading-font"><?php esc_html_e('step', 'motors'); ?> 6</span>
    </div>
	<div class="row stm-relative">
		<div class="container">
			<div class="contact-preferences__title">
				How would you like buyers to contact you about Tesla?
			</div>
			<div class="contact-preferences__options">
				<div class="contact-preferences__item user_email">
					<input type="checkbox" id="user_email" name="user_email" disabled checked>
					<label for="user_email">Email: <?php echo $user->user_email ?></label>
				</div>

		  <?php if ($user_phone): ?>
						<div>
					<div class="contact-preferences__item user-phone">
						<input type="checkbox" id="user_phone" name="user_phone" <?php if ($show_user_phone) echo 'checked' ?> value="1">
						<label for="user_phone">Phone: <?php echo $user_phone; ?></label>
					</div>
					<div class="contact-preferences__sub-items" style="display: <?php echo $show_user_phone ? 'block' : 'none'?> ;">
						<div style="display: flex;">
							<div class="contact-preferences__sub-item user-phone__calls-ok">
								<input type="checkbox" id="user_phone__calls-ok" <?php if ($calls_ok) echo 'checked' ?> name="calls-ok" value="1">
								<label for="user_phone__calls-ok">Calls Ok</label>
							</div>
							<div class="contact-preferences__sub-item user-phone__texts-ok">
								<input type="checkbox" <?php if ($texts_ok) echo 'checked' ?>  id="user_phone__text-ok" name="texts-ok" value="1">
								<label for="user_phone__text-ok">Texts Ok</label>
							</div>
					</div>
					</div>
				</div>
		  <?php endif; ?>

				<div class="contact-preferences__item user_name">
					<input type="checkbox" id="user_name" name="show_user_name" value="1" <?php if ($show_user_name) echo 'checked' ?>>
					<label for="user_name">Display my first name (<?php echo $name; ?>) on the listing</label>
				</div>

			</div>
		</div>
	</div>
</div>
