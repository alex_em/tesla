<?php

if(empty($modern_filter)){
    $modern_filter = false;
}
stm_listings_load_template('loop/start', array('modern' => $modern_filter)); ?>

    <div class="gallery-carousel">
        <?php get_template_part('partials/listing-cars/inventory-image-carousel/loop/list/image'); ?>
    </div>

    <div class="hide_on_mobile">
        <?php stm_listings_load_template('loop/classified/list/image'); ?>
    </div>

    <script>
        jQuery(document).ready(function ($) {
            $('.image-carousel').owlCarousel({
                items: 1,
                nav: true,
                dots: false,
                navText: [
                    '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    '<i class="fa fa-angle-right" aria-hidden="true"></i>'
                ],
            });
        });
    </script>


    <div class="content">
        <div class="meta-top">
            <!--Price-->
            <?php stm_listings_load_template('loop/default/list/price'); ?>
            <!--Title-->
            <?php stm_listings_load_template('loop/default/list/title'); ?>
        </div>

        <!--Item parameters-->
        <div class="meta-middle">
            <?php stm_listings_load_template('loop/default/list/options'); ?>
        </div>

        <!--Item options-->
        <div class="meta-bottom">
            <?php stm_listings_load_template('loop/default/list/features'); ?>
        </div>
    </div>
</div>