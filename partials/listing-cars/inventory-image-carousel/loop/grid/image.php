<?php
$placeholder_path = 'plchldr255.png';
if (wp_is_mobile()) {
    $placeholder_path = 'plchldr350.png';
}
if (stm_is_boats()) {
    $show_compare = get_theme_mod('show_listing_compare', true);

    if (stm_is_boats()) {
        $placeholder_path = 'boats-placeholders/boats-250.png';
    }
}
$placeholder_path = (stm_is_aircrafts()) ? 'ac_plchldr.jpg' : $placeholder_path;
$gallery = get_post_meta(get_the_id(), 'gallery',true);
?>


<div class="image">
    <?php if (has_post_thumbnail()): ?>
        <?php
        $size = 'stm-img-255-135';
        if (wp_is_mobile()) {
            $size = 'stm-img-796-466';
        }
        $car_photos = getPostGalleryUrls($gallery, $size);
        ?>

        <?php if (count($car_photos) > 1): ?>
            <div class="image-carousel">
                <?php foreach ($car_photos as $car_photo): ?>
                    <div class="image-item">
                        <a href="<?php the_permalink() ?>">
                            <img
                                    data-original="<?php echo $car_photo; ?>"
                                    src="<?php echo $car_photo; ?>"
                                    class="lazy img-responsive"
                                    alt="<?php echo stm_generate_title_from_slugs(get_the_id()); ?>"
                            />
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>

        <?php else: ?>
            <?php $img_placeholder = $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), $size); ?>

            <a href="<?php the_permalink() ?>">
                <img
                        data-original="<?php echo esc_url($img[0]); ?>"
                        src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $placeholder_path); ?>"
                        class="lazy img-responsive"
                        alt="<?php echo stm_generate_title_from_slugs(get_the_id()); ?>"
                />
            </a>
        <?php endif; ?>

    <?php else: ?>
        <a href="<?php the_permalink() ?>">
            <img
                    src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/' . $placeholder_path); ?>"
                    class="img-responsive"
                    alt="<?php esc_attr_e('Placeholder', 'motors'); ?>"
            />
        </a>

    <?php endif; ?>
    <?php get_template_part('partials/listing-cars/listing-directory', 'badges'); ?>
</div>