<div class="stm-delete-confirmation-popup stm-disabled">
	<i class="fa fa-close"></i>
	<div class="stm-confirmation-text heading-font">
		<span class="stm-car-title">Are you sure you want to <span class="stm-danger"><?php esc_html_e('Delete', 'motors'); ?></span> all photos?</span>
	</div>
	<div class="actions">
		<a href="#" id="delete" class="button stm-red-btn"><?php esc_html_e('Delete', 'motors'); ?></a>
		<a href="#" id="cancel" class="button stm-grey-btn"><?php esc_html_e('Cancel', 'motors'); ?></a>
	</div>
</div>
<div class="stm-delete-confirmation-overlay stm-disabled"></div>
