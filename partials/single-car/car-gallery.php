<?php
//Getting gallery list
$post_id = get_the_ID();

$gallery = get_post_meta($post_id, 'gallery', true) ? get_post_meta($post_id, 'gallery', true) : [];
$video_preview = get_post_meta($post_id, 'video_preview', true);
$gallery_video = get_post_meta($post_id, 'gallery_video', true);
$special_car = get_post_meta($post_id, 'special_car', true);

$badge_text = get_post_meta($post_id, 'badge_text', true);
$badge_bg_color = get_post_meta($post_id, 'badge_bg_color', true);

if (empty($badge_text)) {
	$badge_text = 'Featured';
}

$badge_style = '';
if (!empty($badge_bg_color)) {
	$badge_style = 'style=background-color:' . $badge_bg_color . ';';
}

if (has_post_thumbnail()) {
	$post_thumbnail_id = get_post_thumbnail_id(get_the_ID());
	array_unshift($gallery, $post_thumbnail_id);
}
$photos = formPhotoSwipeGallery($gallery);
?>

<?php if (!has_post_thumbnail() and stm_check_if_car_imported($post_id)): ?>
	<img
		src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/images/automanager_placeholders/plchldr798automanager.png'); ?>"
		class="img-responsive"
		alt="<?php esc_attr_e('Placeholder', 'motors'); ?>"
	/>
<?php endif; ?>

<div class="stm-car-carousels">

    <!--New badge with videos-->
	<?php $car_media = stm_get_car_medias($post_id); ?>
	<?php if (!empty($car_media['car_videos_count']) and $car_media['car_videos_count'] > 0): ?>
			<div class="stm-car-medias">
            <div class="stm-listing-videos-unit stm-car-videos-<?php echo get_the_id(); ?>">
                <i class="fa fa-film"></i>
                <span><?php echo esc_html($car_media['car_videos_count']); ?><?php esc_html_e('Video', 'motors'); ?></span>
            </div>
        </div>

			<script>
            jQuery(document).ready(function () {

							jQuery(".stm-car-videos-<?php echo get_the_id(); ?>").on('click', function () {
								jQuery(this).lightGallery({
									dynamic: true,
									dynamicEl: [
					  <?php foreach($car_media['car_videos'] as $car_video): ?>
										{
											src: "<?php echo esc_url($car_video); ?>",
										},
					  <?php endforeach; ?>
									],
									download: false,
									mode: 'lg-fade',
								})
							}) //click
						}) //ready

        </script>
	<?php endif; ?>

	<?php if (!empty($special_car) and $special_car == 'on'): ?>
			<div class="special-label h5" <?php echo esc_attr($badge_style); ?>>
            <?php stm_dynamic_string_translation_e('Special Badge Text', $badge_text); ?>
        </div>
	<?php endif; ?>
	<div class="stm-big-car-gallery">
	  <?php if (!empty($video_preview) and !empty($gallery_video)): ?>
		<?php $src = wp_get_attachment_image_src($video_preview, 'stm-img-796-466'); ?>
		<?php if (!empty($src[0])): ?>
				<div class="stm-single-image video-preview"
						 data-id="big-image-<?php echo esc_attr($video_preview); ?>">
                    <a data-fancybox="gallery" class="fancy-iframe" data-iframe="true"
											 data-src="<?php echo esc_url($gallery_video); ?>">
                        <img src="<?php echo esc_url($src[0]); ?>" class="img-responsive"
														 alt="<?php esc_attr_e('Video preview', 'motors'); ?>"/>
                    </a>
                </div>
		<?php endif; ?>
	<?php endif; ?>

	  <?php
	  $k = 0;
	  foreach ($gallery as $gallery_image): $k++;
		  ?>
		  <?php $src = wp_get_attachment_image_src($gallery_image, 'stm-img-796-466'); ?>
		  <?php $full_src = wp_get_attachment_image_src($gallery_image, 'full'); ?>

				<div class="item">
							<a href="<?php echo esc_url($full_src[0]); ?>">
									<img src="<?php echo esc_url($src[0]); ?>"
											 alt="<?php printf(esc_attr__('%s full', 'motors'), get_the_title($post_id)); ?>"/>
							</a>
						</div>
	  <?php endforeach; ?>


	  <?php if (!empty($car_media['car_videos_posters']) and !empty($car_media['car_videos'])): ?>
		  <?php foreach ($car_media['car_videos_posters'] as $k => $val):
			  $src = wp_get_attachment_image_src($val, 'stm-img-350-205');
			  $videoSrc = (isset($car_media['car_videos'][$k])) ? $car_media['car_videos'][$k] : '';
			  if (!empty($src[0])): ?>
								<div class="stm-single-image video-preview" data-id="big-image-<?php echo esc_attr($val); ?>">
                        <a class="fancy-iframe" data-iframe="true" data-src="<?php echo esc_url($videoSrc); ?>">
                            <img src="<?php echo esc_url($src[0]); ?>" class="img-responsive"
																 alt="<?php esc_attr_e('Video preview', 'motors'); ?>"/>
                        </a>
                    </div>
			  <?php endif; ?>
		  <?php endforeach; ?>
	  <?php endif; ?>

    </div>
</div>
<?php if (has_post_thumbnail()): $i = 0;?>
	<div class="custom-thumbnails">
			<?php foreach ($gallery as $gallery_image_id):
			$i++;
			$gallery_image = wp_get_attachment_image_src($gallery_image_id, 'stm-img-350-205')[0]; ?>
					<div class="image_wrapper <?php echo ($i - 1 == 0) ? 'active' : ''; ?>">
                <div>
                    <img src="<?php echo $gallery_image; ?>" alt="">
                </div>
            </div>
		<?php endforeach; ?>
    </div>
		<div id="contact-seller" class="contact-seller">
			<button type="button" class="btn contact-seller__btn">Contact Seller</button>
		</div>
<?php endif; ?>

<script>
	jQuery(document).ready(function () {
		var owlOptions = {
				items: 1,
				smartSpeed: 250,
				dots: false,
				nav: false,
				margin: 0,
				autoplay: false,
				loop: false,
				responsiveRefreshRate: 1000
			},
			pswpOptions = {
				closeEl: true,
				fullscreenEl: false,
				history: false,
				shareEl: false,
				closeOnScroll: false,
				bgOpacity: 0.9,
				tapToClose: true,
				preload: [2, 4],
				showAnimationDuration: 250,
				loadingIndicatorDelay: 1000,
				clickToCloseNonZoomable: false,
			};

		var closeElClasses = ['item', 'caption', 'zoom-wrap', 'ui', 'top-bar']
		var big = $('.stm-big-car-gallery')
		var customImages = $('.custom-thumbnails .image_wrapper')
		var index = 0
		var gallery;

		function openGallery($gallery, index, items, pswpOptions) {
			var $pswp = $(".pswp"),
				owl = $gallery.data("owlCarousel")

			// Options
			var options = $.extend(true, {
				// index
				index: index,

				// Thumbnail
				getThumbBoundsFn: function (index) {
					var $thumbnail = $('.item').eq(index).find("img"),
						offset = $thumbnail.offset();

					return {
						x: offset.left,
						y: offset.top,
						w: $thumbnail.outerWidth()
					};
				},
				getDoubleTapZoom: function (isMouseClick, item) {
					return item.initialZoomLevel;
				},
				zoomEl: false
			}, pswpOptions);

			// PhotoSwipe
			gallery = new PhotoSwipe($pswp.get(0), PhotoSwipeUI_Default, items, options);
			gallery.listen('afterInit', function() {
				calculateArrowPosition()
				calculateCloseBtnPosition()
			});
			gallery.init();

			$('.pswp__button--prev').click(function(){
				gallery.prev()
			})

			$('.pswp__button--next').click(function(){
				gallery.next()
			})

			gallery.listen('afterChange', function () {
				calculateArrowPosition()
				calculateCloseBtnPosition()
			})

			gallery.listen("close", function () {
				$('.owl-stage').addClass('active')
				big.trigger('to.owl.carousel', [this.getCurrentIndex(), 0, true])
				this.currItem.initialLayout = options.getThumbBoundsFn(this.getCurrentIndex());
			});
		}
		$(window).resize(function(){
			calculateArrowPosition()
			calculateCloseBtnPosition()
		})

		function calculateArrowPosition(){
			const index =  gallery.getCurrentIndex();
			const currentActiveItem = gallery.items.find((el, index2) => index2 == index)

			const { container, w } =  currentActiveItem
			const img = $(container).find('img')
			const imgclientWidth = w,
				arrowBtn = $('.pswp__button--arrow')

			const diff = ($(window).width() - imgclientWidth) / 2
			$('.pswp__button--prev').css({
				right: diff - arrowBtn.width() - 10 + 'px'
			})
			$('.pswp__button--next').css({
				left: diff - arrowBtn.width() - 10 + 'px'
			})
		}

		function calculateCloseBtnPosition(){
			setTimeout(() => {
				const currentActiveItem = gallery.currItem,
					{ container } =  currentActiveItem,
					img = container.querySelector('img'),
					windowWidth = $(window).width(),
					windowHeight = $(window).height(),
					closeBtn = $('.pswp__button.pswp__button--close')

				let imgWidth, imgHeight;
				if(typeof  img.naturalWidth == 'undefined'){
					imgWidth =  img.style.width.replace('px', '')
					imgHeight = img.style.height.replace('px', '')
				}else{
					imgWidth =  Math.min(img.style.width.replace('px', ''), img.naturalWidth)
					imgHeight = Math.min(img.style.height.replace('px', ''), img.naturalHeight)
				}

				closeBtn.css({
					top: windowHeight - ( (windowHeight - imgHeight + closeBtn.height() + 40 ) / 2 ) + 'px',
					left: windowWidth - ( (windowWidth - imgWidth + closeBtn.width() + 40 ) / 2 ) + 'px'
				});
			}, 400)

		}

		function initializeGallery($elem, owlOptions, pswpOptions) {
			$elem.each(function (i) {
				var $gallery = $(this),
					uid = i + 1,
					items = <?php echo json_encode($photos)?>,
					options = $.extend(true, {}, pswpOptions);

				// OwlCarousel
				$gallery.owlCarousel(owlOptions);

				// Options
				options.galleryUID = uid;
				$gallery.attr("data-pswp-uid", uid);

				// PhotoSwipe
				$gallery.find(".owl-item").on("click", function (e) {
					e.preventDefault()
					if (!$(e.target).is("img")) return;

					// items PhotoSwipe.init()
					openGallery($gallery, $(this).index(), items.concat(), options);
					return false;
				});
			});
		}

		initializeGallery(big, owlOptions, pswpOptions);

		big.on('drag.owl.carousel', function () {
			$(this).find('.owl-stage').removeClass('active')
		})

		customImages.click(function (event) {
			$('.custom-thumbnails .image_wrapper').removeClass('active')
			$(this).addClass('active')
			index = $(this).index()
			$('.owl-stage').addClass('active')
			big.trigger('to.owl.carousel', [index, 0, true])
		});

		$('#contact-seller').click(function(){
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#stm_dealer_car_info-2").offset().top - 40
			}, 800);
		})
	})
</script>

