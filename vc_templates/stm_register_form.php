<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$link = $atts['link'];

if(!empty($link)) {
	$link = vc_build_link( $link );
}

wp_enqueue_script('stm_recaptchav3');

$login_page = get_theme_mod('custom_login_page');
if (!$login_page) {
	$login_page = get_theme_mod( 'login_page', 1718);
}
?>
<div class="stm-login-register-form <?php echo esc_attr($css_class); ?>">
	<div class="container">
		<h3><?php esc_html_e('Sign Up', 'motors'); ?></h3>
		<div class="stm-register-form">
			<form method="post">
				<?php do_action( 'stm_before_signup_form' ) ?>
				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('First Name', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_user_first_name" placeholder="<?php esc_attr_e('Enter First name', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Last Name', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_user_last_name" placeholder="<?php esc_attr_e('Enter Last name', 'motors') ?>"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('Phone number', 'motors'); ?></h4>
						<input class="user_validated_field" type="tel" name="stm_user_phone" placeholder="<?php esc_attr_e('Enter Phone', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Email *', 'motors'); ?></h4>
						<input class="user_validated_field" type="email" name="stm_user_mail" placeholder="<?php esc_attr_e('Enter E-mail', 'motors') ?>"/>
					</div>
				</div>

				<div class="row form-group">
					<div class="col-md-6">
						<h4><?php esc_html_e('Login *', 'motors'); ?></h4>
						<input class="user_validated_field" type="text" name="stm_nickname" placeholder="<?php esc_attr_e('Enter Login', 'motors') ?>"/>
					</div>
					<div class="col-md-6">
						<h4><?php esc_html_e('Password *', 'motors'); ?></h4>
						<div class="stm-show-password">
							<i class="fa fa-eye-slash"></i>
							<input class="user_validated_field" type="password" name="stm_user_password"  placeholder="<?php esc_attr_e('Enter Password', 'motors') ?>"/>
						</div>
					</div>
				</div>

				<div class="form-group form-checker" style="display: flex; justify-content: space-between">
					<label class="stm_accept_terms">
						<input type="checkbox" name="stm_accept_terms" />
						<span>
              <?php esc_html_e('I agree to the', 'motors'); ?>
							<?php if(!empty($link) and !empty($link['url'])): ?>
								<a href="<?php echo esc_url($link['url']); ?>" target="_blank"><?php esc_html_e($link['title'], 'motors') ?></a>
							<?php endif; ?>
							</span>
					</label>
                    <div style="font-size: 15px;" class="have_account">
										<?php esc_html_e('Already have an account?'); ?>
																<a href="<?php echo get_permalink(esc_attr($login_page))?>">
											<?php esc_html_e('Login')?>
                        </a>
                    </div>
				</div>

				<div class="form-group form-group-submit clearfix">
					<input  type="submit" value="<?php esc_html_e('Sign up now!', 'motors'); ?>" disabled/>
                    <?php if($atts['redirect_page']):?>
                        <input type="hidden" name="redirect_page" value="<?php echo esc_attr($atts['redirect_page'])?>">
                    <?php endif;?>
					<span class="stm-listing-loader"><i class="stm-icon-load1"></i></span>
				</div>

				<div class="stm-validation-message"></div>
				<?php do_action( 'stm_after_signup_form' ) ?>

			</form>
		</div>
	</div>
</div>
