let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
mix
	.setPublicPath('assets')
	.copy('css/algolia-min.css', 'assets/css')
	.copy('images/*', 'assets/images')
	.copy('js/cleave/*', 'assets/js/cleave/')
	//.copy('js/cleave/addons/*', 'assets/js/cleave/addons')
	.copy('js/vendor/*', 'assets/js/vendor/')
	.copy('js/vin-validator/*', 'assets/js/vin-validator/')
	.copy('js/abid-script.js', 'assets/js')
	.copy('js/animatescroll.min.js', 'assets/js')
	.copy('js/child-google-places.js', 'assets/js')
	.copy('js/deff.js', 'assets/js')
	.js('js/slide-in-panel.js', 'assets/js')
	.js('js/cascading-dropdown.js', 'assets/js')
	.js('js/custom.js', 'assets/js')
	.js('js/stm-ajax.js', 'assets/js')
	.js('js/sell-a-car.js', 'assets/js')
	.js('js/wc-cart-child.js', 'assets/js')
	.js('js/tinymce.js', 'assets/js')
	.js('js/src/*', 'assets/js/src/')
	.js('js/add-listing.js', 'assets/js')
	.sass('css/main.scss', 'assets/css')
	.sass('css/tiny.scss', 'assets/css')
	.version()
	.sourceMaps(false)
